#pragma once

#define CX_PSG_RX 33
#define CX_PSG_TX 32

// stdlib
#include <memory>

// platform libs
#include <Arduino.h>

class SerialPorts
{
  public:
  using SharedPointer = std::shared_ptr<SerialPorts>;

  SerialPorts(const int c, const int p)
      : Console(c),
        PSG(p)
  {
    PSG.begin(115200, SERIAL_8N1, CX_PSG_RX, CX_PSG_TX);
  }

  HardwareSerial Console;
  HardwareSerial PSG;
};
