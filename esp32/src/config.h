#pragma once

// stdlib
// #include <iomanip>
// #include <iostream>
#include <memory>
#include <stdint.h>

// platform libs
#include <Arduino.h>

// app parts
#include "types.h"

struct DACEnable
{
  bool value = true;
  bool gate = true;
  bool trigger = true;
  bool gain2x = false;
  bool pitchbend = false;
};

enum class DACMode
{
  NOTE,
  VELOCITY,
  CC7,
  CC14
};

/**
 * @brief DAC channel config
 * 
 */
struct DACConfig
{
  uint8_t channel = 1;
  DACEnable enable;
  DACMode mode = DACMode::NOTE;
  uint8_t min = 0;
  uint8_t max = 127;
  uint8_t CC[2] = {10, 11};
};

/**
 * @brief PSG Voice (oscillator) channel config
 * 
 */
struct PSGVConfig
{
  uint8_t channel = 1;
  bool enable = true;
};

enum class PSGNMode
{
  NOTE,
  VELOCITY,
  CC7
};

/**
 * @brief PSG Noise channel config
 * 
 */
struct PSGNConfig
{
  uint8_t channel = 1;
  uint8_t mixmask;
  PSGNMode mode = PSGNMode::NOTE;
  uint8_t CC[1] = {20};
};

/**
 * @brief PSC Device MIDI mapping configuration
 * 
 */
class Config
{
public:
  using SharedPointer = std::shared_ptr<Config>;

  DACConfig dac[4];
  PSGVConfig psgv[3];
  PSGNConfig psgn[1];

  // Persistence

  void setup();
  void load(const String &name);
  void save(const String &name);

  // DAC

  void setDacChannel(const int num, const uint8_t channel);
  void setDacEnableValue(const int num, const bool enable);
  void setDacEnableGate(const int num, const bool enable);
  void setDacEnableTrigger(const int num, const bool enable);
  void setDac2xGain(const int num, const bool enable);
  void setDacPitchbend(const int num, const bool enable);
  void setDacMin(const int num, const uint8_t val);
  void setDacMax(const int num, const uint8_t val);
  void setDacMode(const int num, const DACMode mode);
  void setDacCC7(const int num, const uint8_t cc);
  void setDacCC14(const int num, const uint8_t cc);

  // PSGV

  void setPsgvChannel(const int num, const uint8_t channel);
  void setPsgvEnable(const int num, const bool enable);

  // PSGN

  void setPsgnChannel(const int num, const uint8_t channel);
  void setPsgnMix(const int num, const uint8_t mixmask);
  void setPsgnMode(const int num, const PSGNMode mode);
  void setPsgnCC7(const int num, const uint8_t cc);

  // Protocol 0x00 config parser

  enum class Protocol0Command
  {
    SET_CHANNEL = 0x0,
    SET_ENABLE = 0x1,
    SET_MODE = 0x2,
    SET_MIN = 0x3,
    SET_MAX = 0x4,
    SET_CC7 = 0x5,
    SET_CC14 = 0x6,

    UNKNOWN = 0xFF
  };

  void processConfig0(const Protocol0Command cmd, const uint8_t dacmask, const uint8_t psgmask, const uint8_t value);
  void parseProtocol0(const ByteVector &message);
};
