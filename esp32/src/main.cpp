// stdlib
#include <memory>

// platform libs
#include <Arduino.h>
#include <SPI.h>
#include <driver/gpio.h>

// app parts
#include "config.h"
#include "midimanager.h"
#include "ports.h"
#include "views.h"

// #if BO
// #endif

class PSC
{
public:
  using SharedPointer = std::shared_ptr<PSC>;

  PSC()
      : m_config(std::make_shared<Config>()),
        m_views(std::make_shared<ViewsContainer>(Wire)),
        m_ports(std::make_shared<SerialPorts>(0, 2)),
        m_midi(std::make_shared<MIDIManager>(m_ports))
  {
  }

  void setup()
  {
    m_config->setup();

    SPI.begin(TFT_SCLK, TFT_MISO, TFT_MOSI, TFT_CS);
    SPI.setFrequency(SPI_FREQUENCY);
    SPI.setHwCs(true);
    delay(100);

    String bootmsg = "Booting...                 ";
    m_views->tft.setup();
    delay(100);
    m_views->tft.displayMessage(bootmsg);
    delay(300);

    bootmsg += "... IO setup ...           ";
    // no need to call ConsoleSerial.begin, MIDI did this for us
    m_views->tft.displayMessage(bootmsg);
    delay(300);
    Wire.begin(SDA, SCL, 400000);
    delay(100);

    bootmsg += "... MIDI setup ...         ";
    m_midi->setup();
    m_midi->allNotesOff()->connect_member(this, &PSC::on_allNotesOff);
    m_midi->systemExclusive()->connect_member(this, &PSC::on_systemExclusive);
    m_midi->noteOn()->connect_member(this, &PSC::on_noteOn);
    m_midi->noteOff()->connect_member(this, &PSC::on_noteOff);
    m_midi->controlChange()->connect_member(this, &PSC::on_controlChange);
    m_midi->pitchBend()->connect_member(this, &PSC::on_pitchBend);
    m_views->tft.displayMessage(bootmsg);
    delay(300);

    m_config->load("default");

    bootmsg += "... Ready !                ";
    bootmsg += "Serial speed is " + String(SERIAL_SPEED);
    m_views->tft.displayMessage(bootmsg);
    delay(1500);

    m_views->tft.displayConfig(m_config);
  }

  void loop()
  {
    if (m_midi)
    {
      m_midi->read();
    }
  }

private: // Internal actions
  void on_allNotesOff()
  {
    for (int num = 0; num < 4; ++num)
    {
      m_dacState.dac[num].gate = false;
      m_dacState.dac[num].trig = false;
    }
    m_psg.allOff();
    const auto frame = m_psg.getState();
    m_views->dac.update(m_dacState);
    m_views->step.update(m_dacState);
    m_ports->PSG.write(frame.data(), frame.size());
  }

  void on_systemExclusive(const ByteVector &message)
  {
    const auto sm = message.cbegin();
    const auto em = message.cend();

    // inspect the message header
    // [Extended manufacturer, Manufacturer byte1, Manufacturer byte2, device ID, protocol ID];
    const ByteVector href{0x00, 0x60, 0x00, 0x00, 0x00};
    if (std::equal(sm, sm + 5, href.cbegin()))
    {
      // chop out the data from the sysex message and consume it
      const ByteVector data(sm + 5, em);
      m_config->parseProtocol0(data);
      m_views->dac.setGain2x(
          m_config->dac[0].enable.gain2x,
          m_config->dac[1].enable.gain2x,
          m_config->dac[2].enable.gain2x,
          m_config->dac[3].enable.gain2x);
      m_views->tft.displayConfig(m_config);
      m_config->save("/default.conf");
    }
  }

  void on_noteOn(byte channel, byte note, byte velocity)
  {
    bool dacupdate = false;
    for (int num = 0; num < 4; ++num)
    {
      if (m_config->dac[num].channel == channel)
      {
        if (m_config->dac[num].mode == DACMode::NOTE)
        {
          m_dacState.dac[num].gate = m_config->dac[num].enable.gate;
          m_dacState.dac[num].trig = m_config->dac[num].enable.trigger;
          if (m_config->dac[num].enable.value)
          {
            const float rn = m_config->dac[num].max - m_config->dac[num].min;
            const float sv = (note - m_config->dac[num].min) / rn;
            m_dacState.dac[num].value = sv * 0xFFF;
          }
          dacupdate = true;
        }
        if (m_config->dac[num].mode == DACMode::VELOCITY)
        {
          m_dacState.dac[num].gate = m_config->dac[num].enable.gate;
          m_dacState.dac[num].trig = m_config->dac[num].enable.trigger;
          if (m_config->dac[num].enable.value)
          {
            const float rn = m_config->dac[num].max - m_config->dac[num].min;
            const float sv = (velocity - m_config->dac[num].min) / rn;
            m_dacState.dac[num].value = sv * 0xFFF;
          }
          dacupdate = true;
        }
      }
      else
      {
        m_dacState.dac[num].trig = false;
        dacupdate = true;
      }
    }

    bool psgupdate = false;
    for (int num = 0; num < 3; ++num)
    {
      if (m_config->psgv[num].enable && m_config->psgv[num].channel == channel)
      {
        psgupdate |= m_psg.noteOn(num, note, velocity);
      }
    }
    if (m_config->psgn[0].channel == channel && (m_config->psgn[0].mixmask & 0x1))
    {
      if (m_config->psgn[0].mode == PSGNMode::NOTE)
      {
        psgupdate |= m_psg.noiseOn(note >> 2, m_config->psgn[0].mixmask >> 1);
      }
      if (m_config->psgn[0].mode == PSGNMode::VELOCITY)
      {
        psgupdate |= m_psg.noiseOn(velocity >> 2, m_config->psgn[0].mixmask >> 1);
      }
    }

    if (dacupdate)
    {
      m_views->dac.update(m_dacState);
      m_views->step.update(m_dacState);
    }
    if (psgupdate)
    {
      const auto frame = m_psg.getState();
      m_ports->PSG.write(frame.data(), frame.size());
    }
  }

  void on_noteOff(byte channel, byte note, byte velocity)
  {
    bool dacupdate = false;
    for (int num = 0; num < 4; ++num)
    {
      if (m_config->dac[num].channel == channel)
      {
        m_dacState.dac[num].trig = false;
        m_dacState.dac[num].gate = false;
        dacupdate = true;
      }
    }

    bool psgupdate = false;
    for (int num = 0; num < 3; ++num)
    {
      if (m_config->psgv[num].enable && m_config->psgv[num].channel == channel)
      {
        psgupdate |= m_psg.noteOff(num);
      }
    }

    if (m_config->psgn[0].channel == channel)
    {
      if (m_config->psgn[0].mode == PSGNMode::NOTE)
      {
        psgupdate |= m_psg.noiseOn(note >> 2, 0);
      }
      if (m_config->psgn[0].mode == PSGNMode::VELOCITY)
      {
        psgupdate |= m_psg.noiseOn(velocity >> 2, 0);
      }
    }

    if (dacupdate)
    {
      m_views->step.update(m_dacState);
      m_views->dac.update(m_dacState);
    }
    if (psgupdate)
    {
      const auto frame = m_psg.getState();
      m_ports->PSG.write(frame.data(), frame.size());
    }
  }

  void on_controlChange(byte channel, byte number, byte value)
  {
    bool dacupdate = false;
    for (int num = 0; num < 4; ++num)
    {
      const bool isEnabled = m_config->dac[num].channel == channel && m_config->dac[num].enable.value;
      const bool isCCMode = m_config->dac[num].mode == DACMode::CC7 || m_config->dac[num].mode == DACMode::CC14;

      if (isEnabled && isCCMode)
      {
        if (m_config->dac[num].CC[0] == number)
        {
          // Stash this value in case CC14 needs it
          m_dacState.dac[num].cc7value = value & 0x7F;

          if (m_config->dac[num].mode == DACMode::CC7)
          {
            const float rn = m_config->dac[num].max - m_config->dac[num].min;
            const float sv = (value - m_config->dac[num].min) / rn;
            m_dacState.dac[num].trig = true;
            m_dacState.dac[num].value = sv * 0xFFF;
            dacupdate = true;
          }
        }
        if (m_config->dac[num].mode == DACMode::CC14 && m_config->dac[num].CC[1] == number)
        {
          m_dacState.dac[num].trig = true;
          m_dacState.dac[num].value = ((m_dacState.dac[num].cc7value << 7) | (value & 0x7F)) >> 2;
          dacupdate = true;
        }
      }
    }

    bool psgupdate = false;
    if (m_config->psgn[0].channel == channel && (m_config->psgn[0].mixmask & 0x1))
    {
      if (m_config->psgn[0].mode == PSGNMode::CC7 && m_config->psgn[0].CC[0] == number)
      {
        psgupdate |= m_psg.noiseOn(value >> 2, m_config->psgn[0].mixmask >> 1);
      }
    }

    if (dacupdate)
    {
      m_views->dac.update(m_dacState);
      m_views->step.update(m_dacState);
    }
    if (psgupdate)
    {
      const auto frame = m_psg.getState();
      m_ports->PSG.write(frame.data(), frame.size());
    }
  }

  void on_pitchBend(byte channel, int value)
  {
    bool dacupdate = false;
    for (int num = 0; num < 4; ++num)
    {
      const bool isEnabled = m_config->dac[num].channel == channel && m_config->dac[num].enable.value;
      const bool isNoteMode = m_config->dac[num].mode == DACMode::NOTE;

      if (isEnabled && isNoteMode)
      {
        if (m_config->dac[num].enable.pitchbend)
        {
          m_dacState.dac[num].pitchbend = value;
          dacupdate = true;
        }
        else if (m_dacState.dac[num].pitchbend != 0)
        {
          m_dacState.dac[num].pitchbend = 0;
          dacupdate = true;
        }
      }
      else if (m_dacState.dac[num].pitchbend != 0)
      {
        m_dacState.dac[num].pitchbend = 0;
        dacupdate = true;
      }
    }

    if (dacupdate)
    {
      m_views->dac.update(m_dacState);
      m_views->step.update(m_dacState);
    }
  }

private: // Internal details
  // App state
  Config::SharedPointer m_config;
  DACState m_dacState;

  // Outputs
  ViewsContainer::SharedPointer m_views;

  // Hardware devices
  SerialPorts::SharedPointer m_ports;
  MIDIManager::SharedPointer m_midi;
  AY38910 m_psg;
};

PSC::SharedPointer Application;

// Main setup
void setup()
{
  Application = std::make_shared<PSC>();
  if (Application)
  {
    Application->setup();
  }
}

// Main loop
void loop()
{
  if (Application)
  {
    Application->loop();
  }
  yield();
}
