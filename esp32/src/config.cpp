#include <map>

#include "config.h"
#include "file.h"

// Persistence

void Config::setup()
{
  psc::fs::begin();
}

void Config::load(const String &name)
{
  const auto cfg = psc::fs::read("/default.conf");

  // If not exists, set the defaults
  if (cfg.size() == 0)
  {
    // Default data is written in sysex protocol 0,
    // because it's more concise :)
    // Set each output to sequential MIDI channels, 1-8:
    parseProtocol0({0x00, 0x01, 0x00, 0x00,
                    0x00, 0x02, 0x00, 0x01,
                    0x00, 0x04, 0x00, 0x02,
                    0x00, 0x08, 0x00, 0x03,
                    0x00, 0x00, 0x01, 0x04,
                    0x00, 0x00, 0x02, 0x05,
                    0x00, 0x00, 0x04, 0x06,
                    0x00, 0x00, 0x08, 0x07});
    // Enable Value, Gate, Trigger for the DAC outputs A and B, Value only for DAC outputs C and D:
    parseProtocol0({0x01, 0x03, 0x00, 0x07,
                    0x01, 0x0C, 0x00, 0x01});
    // Set the same Min (G1) and Max (D7) for first two DAC outputs:
    parseProtocol0({0x03, 0x03, 0x00, 20,
                    0x04, 0x03, 0x00, 70});
    // Set the full ranage for second two DAC outputs:
    parseProtocol0({0x03, 0x0C, 0x00, 0x00,
                    0x04, 0x0C, 0x00, 0x7F});
    // Set third DAC output to CC7, forth DAC output to CC14
    parseProtocol0({0x02, 0x04, 0x00, 0x02,
                    0x02, 0x08, 0x00, 0x03});
    // Set third DAC output CC7 to {10}, fourth DAC output CC14 to {11, 12}
    parseProtocol0({0x05, 0x04, 0x00, 10,
                    0x05, 0x08, 0x00, 11, 0x06, 0x08, 0x00, 12});

    save("/default.conf");
  }
  else
  {
    parseProtocol0(cfg);
  }
}

void Config::save(const String &name)
{
  // NOTE: config serialisation to Sysex is (mostly) duplicated in config-sysex/src/configuration.cpp

  ByteVector data;

  // To reduce the message size, we should iterate the config types and
  // values and group (mask) together the outputs which have the same value

  uint8_t cfg = 0x00; // Channel
  std::map<uint8_t, uint16_t> channel_map;
  for (int i = 0; i < 4; ++i)
  {
    const auto dacchannel = dac[i].channel - 1;
    channel_map[dacchannel] |= 1 << i;

    if (i < 3)
    {
      const auto psgchannel = psgv[i].channel - 1;
      channel_map[psgchannel] |= 1 << (i + 8);
    }
    else
    {
      const auto psgchannel = psgn[0].channel - 1;
      channel_map[psgchannel] |= 1 << (i + 8);
    }
  }
  for (const auto &p : channel_map)
  {
    data.push_back(cfg);
    data.push_back(p.second & 0xFF);
    data.push_back(p.second >> 8);
    data.push_back(p.first);
  }

  cfg = 0x01; // Enable
  std::map<uint8_t, uint16_t> enable_map;
  for (int i = 0; i < 4; ++i)
  {
    const auto dacenable = ((dac[i].enable.value ? 0b00001 : 0b00000) |
                            (dac[i].enable.gate ? 0b00010 : 0b00000) |
                            (dac[i].enable.trigger ? 0b00100 : 0b00000) |
                            (dac[i].enable.gain2x ? 0b01000 : 0b00000) |
                            (dac[i].enable.pitchbend ? 0b10000 : 0b00000));
    enable_map[dacenable] |= 1 << i;

    if (i < 3)
    {
      const auto psgenable = (psgv[i].enable ? 0b0001 : 0b000);
      enable_map[psgenable] |= 1 << (i + 8);
    }
    else
    {
      const auto psgenable = psgn[0].mixmask;
      enable_map[psgenable] |= 1 << (i + 8);
    }
  }
  for (const auto &p : enable_map)
  {
    data.push_back(cfg);
    data.push_back(p.second & 0xFF);
    data.push_back(p.second >> 8);
    data.push_back(p.first);
  }

  cfg = 0x02; // Mode
  std::map<uint8_t, uint16_t> mode_map;
  for (int i = 0; i < 4; ++i)
  {
    const auto dacmode = (uint8_t)dac[i].mode;
    mode_map[dacmode] |= 1 << i;

    if (i == 3)
    {
      const auto psgmode = (uint8_t)psgn[0].mode;
      mode_map[psgmode] |= 1 << (i + 8);
    }
  }
  for (const auto &p : mode_map)
  {
    data.push_back(cfg);
    data.push_back(p.second & 0xFF);
    data.push_back(p.second >> 8);
    data.push_back(p.first);
  }

  cfg = 0x03; // Min - DAC only
  std::map<uint8_t, uint8_t> min_map;
  for (int i = 0; i < 4; ++i)
  {
    const auto dacmin = dac[i].min & 0x7F;
    min_map[dacmin] |= 1 << i;
  }
  for (const auto &p : min_map)
  {
    data.push_back(cfg);
    data.push_back(p.second);
    data.push_back(0x00);
    data.push_back(p.first);
  }

  cfg = 0x04; // Max - DAC only
  std::map<uint8_t, uint8_t> max_map;
  for (int i = 0; i < 4; ++i)
  {
    const auto dacmax = dac[i].max & 0x7F;
    max_map[dacmax] |= 1 << i;
  }
  for (const auto &p : max_map)
  {
    data.push_back(cfg);
    data.push_back(p.second);
    data.push_back(0x00);
    data.push_back(p.first);
  }

  cfg = 0x05; // CC7
  std::map<uint8_t, uint16_t> cc7_map;
  for (int i = 0; i < 4; ++i)
  {
    const auto daccc7 = dac[i].CC[0] & 0x7F;
    cc7_map[daccc7] |= 1 << i;

    if (i == 3)
    {
      const auto psgcc7 = psgn[i].CC[0] & 0x7F;
      cc7_map[psgcc7] |= 1 << (i + 8);
    }
  }
  for (const auto &p : cc7_map)
  {
    data.push_back(cfg);
    data.push_back(p.second & 0xFF);
    data.push_back(p.second >> 8);
    data.push_back(p.first);
  }

  cfg = 0x06; // CC14 - DAC only
  std::map<uint8_t, uint8_t> cc14_map;
  for (int i = 0; i < 4; ++i)
  {
    const auto daccc14 = dac[i].CC[1] & 0x7F;
    cc14_map[daccc14] |= 1 << i;
  }
  for (const auto &p : cc14_map)
  {
    data.push_back(cfg);
    data.push_back(p.second & 0xFF);
    data.push_back(0x00);
    data.push_back(p.first);
  }

  psc::fs::write(name, data);
}

// DAC

void Config::setDacChannel(const int num, const uint8_t channel)
{
  // std::cout << "setDacChannel " << num << std::endl;
  if (num < 0 || num > 3)
    return;
  dac[num].channel = channel;
}

void Config::setDacEnableValue(const int num, const bool enable)
{
  // std::cout << "setDacEnableValue " << num << std::endl;
  if (num < 0 || num > 3)
    return;
  dac[num].enable.value = enable;
}

void Config::setDacEnableGate(const int num, const bool enable)
{
  // std::cout << "setDacEnableGate " << num << std::endl;
  if (num < 0 || num > 3)
    return;
  dac[num].enable.gate = enable;
}

void Config::setDacEnableTrigger(const int num, const bool enable)
{
  // std::cout << "setDacEnableTrigger " << num << std::endl;
  if (num < 0 || num > 3)
    return;
  dac[num].enable.trigger = enable;
}

void Config::setDac2xGain(const int num, const bool enable)
{
  // std::cout << "setDacEnableTrigger " << num << std::endl;
  if (num < 0 || num > 3)
    return;
  dac[num].enable.gain2x = enable;
}

void Config::setDacPitchbend(const int num, const bool enable)
{
  // std::cout << "setDacEnableTrigger " << num << std::endl;
  if (num < 0 || num > 3)
    return;
  dac[num].enable.pitchbend = enable;
}

void Config::setDacMin(const int num, const uint8_t val)
{
  // std::cout << "setDacMin " << num << std::endl;
  if (num < 0 || num > 3)
    return;
  dac[num].min = val;
}

void Config::setDacMax(const int num, const uint8_t val)
{
  // std::cout << "setDacMax " << num << std::endl;
  if (num < 0 || num > 3)
    return;
  dac[num].max = val;
}

void Config::setDacMode(const int num, const DACMode mode)
{
  // std::cout << "setDacMode " << num << std::endl;
  if (num < 0 || num > 3)
    return;
  dac[num].mode = mode;
}

void Config::setDacCC7(const int num, const uint8_t cc)
{
  // std::cout << "setDacCC7 " << num << std::endl;
  if (num < 0 || num > 3)
    return;
  dac[num].CC[0] = cc;
}

void Config::setDacCC14(const int num, const uint8_t cc)
{
  // std::cout << "setDacCC14 " << num << std::endl;
  if (num < 0 || num > 3)
    return;
  dac[num].CC[1] = cc;
}

// PSGV

void Config::setPsgvChannel(const int num, const uint8_t channel)
{
  // std::cout << "setPsgvChannel " << num << std::endl;
  if (num < 0 || num > 2)
    return;
  psgv[num].channel = channel;
}

void Config::setPsgvEnable(const int num, const bool enable)
{
  // std::cout << "setPsgvEnable " << num << std::endl;
  if (num < 0 || num > 2)
    return;
  psgv[num].enable = enable;
}

// PSGN

void Config::setPsgnChannel(const int num, const uint8_t channel)
{
  // std::cout << "setPsgnChannel " << num << std::endl;
  if (num != 0)
    return;
  psgn[num].channel = channel;
}

/**
 * mixmask is 4 bits:
 * [MixC, MixB, MixA, Enable]
 */
void Config::setPsgnMix(const int num, const uint8_t mixmask)
{
  // std::cout << "setPsgnMix " << num << std::endl;
  if (num != 0)
    return;
  psgn[num].mixmask = mixmask;
}

void Config::setPsgnMode(const int num, const PSGNMode mode)
{
  // std::cout << "setPsgnMode " << num << std::endl;
  if (num != 0)
    return;
  psgn[num].mode = mode;
}

void Config::setPsgnCC7(const int num, const uint8_t cc)
{
  // std::cout << "setPsgnCC7 " << num << std::endl;
  if (num != 0)
    return;
  psgn[num].CC[0] = cc;
}

// Protocol 0x00 config parser

void Config::processConfig0(const Protocol0Command cmd, const uint8_t dacmask, const uint8_t psgmask, const uint8_t value)
{
  // std::cout << "processConfig: " << (int)type << " " << (int)dacmask << " " << (int)psgmask << " " << (int)value << std::endl;
  switch (cmd)
  {
  case Protocol0Command::UNKNOWN:
    break;
  case Protocol0Command::SET_CHANNEL:
  {
    for (int i = 0; i < 4; ++i)
    {
      const auto mbit = 1 << i;
      const bool matchDac = (dacmask & mbit) > 0;
      const bool matchPsg = (psgmask & mbit) > 0;
      if (matchDac)
      {
        setDacChannel(i, 1 + (value & 0xF));
      }
      if (matchPsg)
      {
        setPsgvChannel(i, 1 + (value & 0xF));
      }
      if (i == 3 && matchPsg)
      {
        setPsgnChannel(0, 1 + (value & 0xF));
      }
    }
  }
  break;
  case Protocol0Command::SET_ENABLE:
  {
    for (int i = 0; i < 4; ++i)
    {
      const auto mbit = 1 << i;
      const bool matchDac = (dacmask & mbit) > 0;
      const bool matchPsg = (psgmask & mbit) > 0;
      if (matchDac)
      {
        setDacEnableValue(i, (value & 0b00001) > 0);
        setDacEnableGate(i, (value & 0b00010) > 0);
        setDacEnableTrigger(i, (value & 0b00100) > 0);
        setDac2xGain(i, (value & 0b01000) > 0);
        setDacPitchbend(i, (value & 0b10000) > 0);
      }
      if (matchPsg)
      {
        setPsgvEnable(i, (value & 0b0001) > 0);
      }
      if (i == 3 && matchPsg)
      {
        setPsgnMix(0, value & 0b1111);
      }
    }
  }
  break;
  case Protocol0Command::SET_MODE:
  {
    for (int i = 0; i < 4; ++i)
    {
      const auto mbit = 1 << i;
      const bool matchDac = (dacmask & mbit) > 0;
      const bool matchPsg = (psgmask & mbit) > 0;
      if (matchDac)
      {
        switch (value)
        {
        case 0x00: // Note
        {
          setDacMode(i, DACMode::NOTE);
        }
        break;
        case 0x01: // Velocity
        {
          setDacMode(i, DACMode::VELOCITY);
        }
        break;
        case 0x02: // CC7
        {
          setDacMode(i, DACMode::CC7);
        }
        break;
        case 0x03: // CC14
        {
          setDacMode(i, DACMode::CC14);
        }
        break;
        }
      }
      // No mode for PSGV
      if (i == 3 && matchPsg)
      {
        switch (value)
        {
        case 0x00: // Note
        {
          setPsgnMode(0, PSGNMode::NOTE);
        }
        break;
        case 0x01: // Velocity
        {
          setPsgnMode(0, PSGNMode::VELOCITY);
        }
        break;
        case 0x02: // CC7
        {
          setPsgnMode(0, PSGNMode::CC7);
        }
        break;
        case 0x03: // CC14 not valid, use Note
        {
          setPsgnMode(0, PSGNMode::NOTE);
        }
        break;
        }
      }
    }
  }
  break;
  case Protocol0Command::SET_MIN:
  {
    for (int i = 0; i < 4; ++i)
    {
      // DAC only
      const auto mbit = 1 << i;
      const bool matchDac = (dacmask & mbit) > 0;
      if (matchDac)
      {
        setDacMin(i, value & 0x7F);
      }
    }
  }
  break;
  case Protocol0Command::SET_MAX:
  {
    for (int i = 0; i < 4; ++i)
    {
      // DAC only
      const auto mbit = 1 << i;
      const bool matchDac = (dacmask & mbit) > 0;
      if (matchDac)
      {
        setDacMax(i, value & 0x7F);
      }
    }
  }
  break;
  case Protocol0Command::SET_CC7:
  {
    for (int i = 0; i < 4; ++i)
    {
      const auto mbit = 1 << i;
      const bool matchDac = (dacmask & mbit) > 0;
      const bool matchPsg = (psgmask & mbit) > 0;
      if (matchDac)
      {
        setDacCC7(i, value & 0x7F);
      }
      // No CC7 for PSGV
      if (i == 3 && matchPsg)
      {
        setPsgnCC7(0, value & 0x7F);
      }
    }
  }
  break;
  case Protocol0Command::SET_CC14:
  {
    for (int i = 0; i < 4; ++i)
    {
      const auto mbit = 1 << i;
      const bool matchDac = (dacmask & mbit) > 0;
      if (matchDac)
      {
        setDacCC14(i, value & 0x7F);
      }
      // No CC14 for PSGV or PSGN
    }
  }
  break;
  }
}

void Config::parseProtocol0(const ByteVector &message)
{
  enum class PARSESTATE
  {
    WANT_CONFIG_CMD,
    WANT_DAC_MASK,
    WANT_PSG_MASK,
    WANT_VALUE
  } state;

  state = PARSESTATE::WANT_CONFIG_CMD;
  Protocol0Command cmd = Protocol0Command::UNKNOWN;
  uint8_t dacmask = 0;
  uint8_t psgmask = 0;
  uint8_t value = 0;
  for (const auto &b : message)
  {
    // std::cout << "parse byte " << (int)b << " in state " << (int)state << " ";
    if (state == PARSESTATE::WANT_CONFIG_CMD)
    {
      // std::cout << " set type ";
      state = PARSESTATE::WANT_DAC_MASK;
      cmd = static_cast<Protocol0Command>(b);
    }
    else if (state == PARSESTATE::WANT_DAC_MASK)
    {
      // std::cout << " set dac mask ";
      state = PARSESTATE::WANT_PSG_MASK;
      dacmask = b;
    }
    else if (state == PARSESTATE::WANT_PSG_MASK)
    {
      // std::cout << " set psg mask ";
      state = PARSESTATE::WANT_VALUE;
      psgmask = b;
    }
    else if (state == PARSESTATE::WANT_VALUE)
    {
      // std::cout << " set value ";
      value = b;
      processConfig0(cmd, dacmask, psgmask, value);
      state = PARSESTATE::WANT_CONFIG_CMD;
      cmd = Protocol0Command::UNKNOWN;
      dacmask = 0;
      psgmask = 0;
      value = 0;
      // std::cout << " reset ";
    }
    // std::cout << std::endl;
  }
}
