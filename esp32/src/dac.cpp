#include "dac.h"

DACView::DACView(TwoWire &wire, const uint8_t &nld)
    : m_wire(wire),
      m_nld(nld)
{
}

/**
 * Set up the DAC device
 */
void DACView::setup()
{
  // DAC setup
  m_dac.attatch(m_wire, m_nld);
  m_dac.selectVref(MCP4728::VREF::INTERNAL_2_8V, MCP4728::VREF::INTERNAL_2_8V, MCP4728::VREF::INTERNAL_2_8V, MCP4728::VREF::INTERNAL_2_8V);
  m_dac.selectPowerDown(MCP4728::PWR_DOWN::GND_500KOHM, MCP4728::PWR_DOWN::GND_500KOHM, MCP4728::PWR_DOWN::GND_500KOHM, MCP4728::PWR_DOWN::GND_500KOHM);
  m_dac.selectGain(MCP4728::GAIN::X1, MCP4728::GAIN::X1, MCP4728::GAIN::X1, MCP4728::GAIN::X1);
  m_dac.analogWrite(0, 0, 0, 0);
  m_dac.enable(true);
  m_dac.readRegisters();

  delay(100);
}

void DACView::setGain2x(const bool a, const bool b, const bool c, const bool d)
{
  m_dac.selectGain(
      a ? MCP4728::GAIN::X2 : MCP4728::GAIN::X1,
      b ? MCP4728::GAIN::X2 : MCP4728::GAIN::X1,
      c ? MCP4728::GAIN::X2 : MCP4728::GAIN::X1,
      d ? MCP4728::GAIN::X2 : MCP4728::GAIN::X1);
}

void DACView::update(const DACState &ss)
{
  const uint16_t a = std::min(std::max(ss.dac[0].value + ss.dac[0].pitchbend, 0), 0xFFF);
  const uint16_t b = std::min(std::max(ss.dac[1].value + ss.dac[1].pitchbend, 0), 0xFFF);
  const uint16_t c = std::min(std::max(ss.dac[2].value + ss.dac[2].pitchbend, 0), 0xFFF);
  const uint16_t d = std::min(std::max(ss.dac[3].value + ss.dac[3].pitchbend, 0), 0xFFF);
  m_dac.analogWrite(a, b, c, d);
  delayMicroseconds(I2C_DELAY_US);
  m_dac.readRegisters();
  delayMicroseconds(I2C_DELAY_US);
}
