#pragma once

#include <FS.h>
#include <SPIFFS.h>
#include <string>

#include "types.h"

namespace psc
{
  namespace fs
  {
    void begin();
    ByteVector read(const String &path);
    void write(const String &path, const ByteVector &cb);

    ByteVector toByteVector(const String &s);
    String toString(const ByteVector &data);

  } // namespace fs
} // namespace psc
