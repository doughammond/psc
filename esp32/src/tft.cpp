#include "tft.h"

void TFT::setup()
{
  // std::cout << "TFT Setup" << std::endl;
  m_tft.init();
  m_tft.setRotation(3);
  m_tft.setTextSize(0);
  m_tft.fillScreen(TFT_BLACK);
}

void TFT::displayConfig(Config::SharedPointer cfg)
{
  __preDisplay();

  const auto dacColour = [=](const bool gain2x)
  {
    if (gain2x)
    {
      m_tft.setTextColor(TFT_RED);
    }
    else
    {
      m_tft.setTextColor(TFT_ORANGE);
    }
  };

  dacColour(cfg->dac[0].enable.gain2x);
  m_tft.drawString("A", COLS[1], ROWS[0]);
  dacColour(cfg->dac[1].enable.gain2x);
  m_tft.drawString("B", COLS[2], ROWS[0]);
  dacColour(cfg->dac[2].enable.gain2x);
  m_tft.drawString("C", COLS[3], ROWS[0]);
  dacColour(cfg->dac[3].enable.gain2x);
  m_tft.drawString("D", COLS[4], ROWS[0]);

  m_tft.setTextColor(TFT_GREEN);
  m_tft.drawString("1", COLS[5], ROWS[0]);
  m_tft.drawString("2", COLS[6], ROWS[0]);
  m_tft.drawString("3", COLS[7], ROWS[0]);
  m_tft.setTextColor(TFT_CYAN);
  m_tft.drawString("N", COLS[8], ROWS[0]);

  m_tft.drawLine(0, 12, 160, 12, TFT_OLIVE);
  m_tft.setTextColor(TFT_OLIVE);
  m_tft.drawString("Ch.", COLS[0], ROWS[1]);
  m_tft.drawString("En.", COLS[0], ROWS[2]);
  m_tft.drawString("Mo.", COLS[0], ROWS[5]);
  m_tft.drawString("Min", COLS[0], ROWS[6]);
  m_tft.drawString("Max", COLS[0], ROWS[7]);
  m_tft.drawString("CC", COLS[0], ROWS[8]);

  size_t c = 1;
  for (const auto &d : cfg->dac)
  {
    const auto col = COLS[c];
    m_tft.setTextColor(TFT_WHITE);
    m_tft.drawString(String(d.channel), col, ROWS[1]);
    switch (d.mode)
    {
    case DACMode::NOTE:
      m_tft.drawString("N", col, ROWS[5]);
      break;
    case DACMode::VELOCITY:
      m_tft.drawString("V", col, ROWS[5]);
      break;
    case DACMode::CC7:
      m_tft.drawString("7", col, ROWS[5]);
      m_tft.drawString(String(d.CC[0]), col, ROWS[8]);
      break;
    case DACMode::CC14:
      m_tft.drawString("14", col, ROWS[5]);
      m_tft.drawString(String(d.CC[0]), col, ROWS[8]);
      m_tft.drawString(String(d.CC[1]), col, ROWS[9]);
      break;
    }

    if (d.mode == DACMode::NOTE || d.mode == DACMode::VELOCITY || d.mode == DACMode::CC7)
    {
      m_tft.drawString(String(d.min), col, ROWS[6]);
      m_tft.drawString(String(d.max), col, ROWS[7]);
    }

    if (d.enable.trigger)
    {
      m_tft.setTextColor(TFT_WHITE);
    }
    else
    {
      m_tft.setTextColor(TFT_DARKGREY);
    }
    m_tft.drawString("T", col, ROWS[2]);

    // Gate does not apply to CC modes
    if (d.mode == DACMode::NOTE || d.mode == DACMode::VELOCITY)
    {
      if (d.enable.gate)
      {
        m_tft.setTextColor(TFT_WHITE);
      }
      else
      {
        m_tft.setTextColor(TFT_DARKGREY);
      }
      m_tft.drawString("G", col, ROWS[3]);
    }

    if (d.enable.value)
    {
      m_tft.setTextColor(TFT_WHITE);
    }
    else
    {
      m_tft.setTextColor(TFT_DARKGREY);
    }
    m_tft.drawString("V", col, ROWS[4]);

    c++;
  }

  for (const auto &p : cfg->psgv)
  {
    const auto col = COLS[c];
    m_tft.setTextColor(TFT_WHITE);
    m_tft.drawString(String(p.channel), col, ROWS[1]);
    if (!p.enable)
    {
      m_tft.setTextColor(TFT_DARKGREY);
    }
    m_tft.drawString("V", col, ROWS[4]);

    c++;
  }

  for (const auto &p : cfg->psgn)
  {
    const auto col = COLS[c];
    m_tft.setTextColor(TFT_WHITE);
    m_tft.drawString(String(p.channel), col, ROWS[1]);

    switch (p.mode)
    {
    case PSGNMode::NOTE:
      m_tft.drawString("N", col, ROWS[5]);
      break;
    case PSGNMode::VELOCITY:
      m_tft.drawString("V", col, ROWS[5]);
      break;
    case PSGNMode::CC7:
      m_tft.drawString("7", col, ROWS[5]);
      m_tft.drawString(String(p.CC[0]), col, ROWS[8]);
      break;
    }

    if (p.mixmask & 0b0001 && p.mixmask & 0b0010)
    {
      m_tft.setTextColor(TFT_WHITE);
    }
    else
    {
      m_tft.setTextColor(TFT_DARKGREY);
    }
    m_tft.drawString("A", col, ROWS[2]);

    if (p.mixmask & 0b0001 && p.mixmask & 0b0100)
    {
      m_tft.setTextColor(TFT_WHITE);
    }
    else
    {
      m_tft.setTextColor(TFT_DARKGREY);
    }
    m_tft.drawString("B", col, ROWS[3]);

    if (p.mixmask & 0b0001 && p.mixmask & 0b1000)
    {
      m_tft.setTextColor(TFT_WHITE);
    }
    else
    {
      m_tft.setTextColor(TFT_DARKGREY);
    }
    m_tft.drawString("C", col, ROWS[4]);

    c++;
  }
  __postDisplay();
}

void TFT::displaySystemDebug()
{
  __preDisplay();

  const auto temp = temperatureRead();
  const auto freeheapK = ESP.getFreeHeap();
  const auto freepramK = ESP.getFreePsram();

  m_tft.setTextColor(TFT_RED);
  m_tft.drawString(String(freeheapK) + " free heap", COLS[0], ROWS[1]);
  m_tft.drawString(String(freepramK) + " free psram", COLS[0], ROWS[2]);
  m_tft.drawString(String(temp) + " degC", COLS[0], ROWS[3]);

  __postDisplay();
}

void TFT::displayMessage(String msg)
{
  __preDisplay();

  m_tft.setTextColor(TFT_YELLOW);
  m_tft.setTextWrap(true);
  m_tft.setTextDatum(TL_DATUM);
  int ln = 0;
  const int linelen = 27;
  while (msg.length() > 0)
  {
    String line = msg.substring(0, linelen);
    msg.remove(0, linelen);
    m_tft.drawString(line, 0, ln * 9);
    ln++;
  }

  __postDisplay();
}