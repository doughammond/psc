#pragma once

#define CX_NLDAC 16

// stdlib
#include <memory>

// platform libs
#include <Arduino.h>

// app parts
#include "dac.h"
#include "step.h"
#include "tft.h"

class ViewsContainer
{
  public:
  using SharedPointer = std::shared_ptr<ViewsContainer>;

  ViewsContainer(TwoWire &w)
      : step(w),
        dac(w, CX_NLDAC)
  {
    step.setup();
    dac.setup();
  }

  StepView step;
  DACView dac;
  TFT tft;
};
