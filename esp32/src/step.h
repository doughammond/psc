#pragma once

// #include <iostream>

// platform libs
#include <Arduino.h>

// arduino-lib
#include <Adafruit_MCP23X17.h>

// app parts
// #include "ostimer.h"
#include "types.h"

constexpr uint16_t LEDR = 0b0000000100000000;
constexpr uint16_t LEDG = 0b0000001000000000;
constexpr uint16_t LEDB = 0b0000010000000000;
constexpr uint16_t LEDW = 0b0000011100000000;
constexpr uint16_t MTRG = 0b1000000000000000;

class StepView : public View
{
public:
  StepView(TwoWire &wire)
      : m_wire(wire)
  {
  }

public:
  virtual void setup();

  virtual void update(const DACState &ss)
  {
    update(ss, false);
  }
  virtual void update(const DACState &ss, const bool _setLED);

private:
  // I2C driver
  TwoWire &m_wire;

  // OSTimer m_tmr_led;
  uint16_t m_quiescestate;

  // MCP23017 for trig and gate signals, and master RGB LED and clock trig output
  Adafruit_MCP23X17 m_gpio;
};
