#pragma once

// platform libs
#include <Arduino.h>

// arduino-lib
#include <MCP4728.h>

// misc-lib
#include <AY38910.h>

// app parts
#include "ostimer.h"
#include "types.h"

class DACView : public View
{
public:
  DACView(TwoWire &wire, const uint8_t &nld);

public:
  /**
   * Set up the DAC device
   */
  virtual void setup();

  virtual void setGain2x(const bool a, const bool b, const bool c, const bool d);
  virtual void update(const DACState &ss);

private:
  // I2C driver
  TwoWire &m_wire;

  // MCP4728 DAC driver
  uint8_t m_nld;
  MCP4728 m_dac;
};
