#pragma once

// Platform
#include <esp_timer.h>

// misc-lib
#include <Timers.h>

/**
 * OO wrapper/adaptor around the os_timer_t.
 * Facilitates the conversion of the OS timer callback
 * firing into a Signal::emit.
 */
class OSTimer : public ITimer
{
public:
  explicit OSTimer();
  ~OSTimer();

  static void Callback(void *t);

  virtual void run(const uint16_t &interval);

  virtual void stop();

  virtual void single(const uint16_t &interval);

  virtual TriggerSignal::SharedPointer trigger() const;

  virtual const bool armed() const;

  virtual const bool running() const;

private:
  bool m_armed;
  bool m_running;
  esp_timer_handle_t m_tmr;
  TriggerSignal::SharedPointer m_trigger;
};
