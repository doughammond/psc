// Application
#include "ostimer.h"

OSTimer::OSTimer() : m_trigger(new TriggerSignal())
{
  const esp_timer_create_args_t args{
      OSTimer::Callback,
      this,
      ESP_TIMER_TASK,
      "OSTimer"};
  esp_timer_create(
      &args,
      &m_tmr);
}

OSTimer::~OSTimer()
{
  esp_timer_delete(m_tmr);
}

void OSTimer::Callback(void *t)
{
  OSTimer *tmr = static_cast<OSTimer *>(t);
  // TODO: this should un-arm the timer, but no API defined for that
  tmr->trigger()->emit();
}

/**
 * interval is in milliseconds
 */
void OSTimer::run(const uint16_t &interval)
{
  stop();
  if (interval > 0)
  {
    esp_timer_start_periodic(m_tmr, interval * 1000);
    m_running = true;
  }
  // cannot "run" with zero interval
}

void OSTimer::stop()
{
  esp_timer_stop(m_tmr);
  m_armed = false;
  m_running = false;
}

void OSTimer::single(const uint16_t &interval)
{
  stop();
  // only bother invoking OS timer if there is an interval
  if (interval > 0)
  {
    esp_timer_start_once(m_tmr, interval * 1000);
    m_armed = true;
  }
  // interval 0 means fire immediately
  else
  {
    m_trigger->emit();
  }
}

TriggerSignal::SharedPointer OSTimer::trigger() const
{
  return m_trigger;
}

const bool OSTimer::armed() const
{
  return m_armed;
}

const bool OSTimer::running() const
{
  return m_running;
}