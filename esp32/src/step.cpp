#include "step.h"

void StepView::setup()
{
  // GPIO expander setup
  m_gpio.begin_I2C(0, &m_wire);

  // gates
  m_gpio.pinMode(0, OUTPUT);
  m_gpio.pinMode(1, OUTPUT);
  m_gpio.pinMode(2, OUTPUT);
  m_gpio.pinMode(3, OUTPUT);

  // trigs
  m_gpio.pinMode(4, OUTPUT);
  m_gpio.pinMode(5, OUTPUT);
  m_gpio.pinMode(6, OUTPUT);
  m_gpio.pinMode(7, OUTPUT);

  // rgb led
  m_gpio.pinMode(8, OUTPUT);
  m_gpio.pinMode(9, OUTPUT);
  m_gpio.pinMode(10, OUTPUT);
  // master trig
  m_gpio.pinMode(11, OUTPUT);

  // connect the LED timer trigger to GPIO state set
  // m_tmr_led.trigger()->connect(
  //     [=]()
  //     { m_gpio.writeGPIOAB(m_quiescestate); });
}

// XXX ESP32 i2c is very sensitive to how fast you can send commands; this needs some tuning (without using delay())
void StepView::update(const DACState &ss, const bool _setLED)
{
  // channel gates on GPIO A [0:3] & channel trigs on GPIO A [4:7]
  const uint16_t ga = (ss.dac[3].gate << 3) | (ss.dac[2].gate << 2) | (ss.dac[1].gate << 1) | (ss.dac[0].gate);
  const uint16_t ta = (ss.dac[3].trig << 7) | (ss.dac[2].trig << 6) | (ss.dac[1].trig << 5) | (ss.dac[0].trig << 4);

  // if (setLED)
  // {
  //   // // RGBLED on GPIO B [0:2]; white on 1 / green on 4's / red on others
  //   // const uint16_t gb = ss.step == 0 ? (LEDW) : ((ss.step % 4) == 0 ? (LEDG) : (LEDR));
  //   // // master trig on GPIO B [7]
  //   // const uint16_t tb = MTRG;

  //   // // all gates, trigs and LED on initally
  //   // m_gpio.writeGPIOAB(ga | ta | gb | tb);
  //   // delayMicroseconds(I2C_DELAY_US);
  //   // // trigs turn off immediately, leaves gates and LED active
  //   // m_gpio.writeGPIOAB(ga | gb);
  //   // delayMicroseconds(I2C_DELAY_US);
  //   // // std::cout << "StepView setLED " << (ga | gb) << std::endl;

  //   // // RGBLED turn off in a short while, leaves gates active
  //   // m_quiescestate = ga;
  //   // m_tmr_led.single(25);
  // }
  // else
  {
    // master trig on GPIO B [7]
    const uint16_t tb = MTRG;

    // all gates, trigs on initally
    m_gpio.writeGPIOAB(ga | ta | tb);
    delayMicroseconds(I2C_DELAY_US);

    // trigs turn off immediately, leaves gates active
    m_gpio.writeGPIOAB(ga);
    delayMicroseconds(I2C_DELAY_US);
    // std::cout << "StepView !setLED " << (ga) << std::endl;

    // leave gates active
    m_quiescestate = ga;
  }
}