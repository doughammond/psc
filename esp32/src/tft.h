#pragma once

// arduino-lib
#include <TFT_eSPI.h>

// app
#include "config.h"

constexpr uint8_t ROWS[] = {9, 25, 38, 51, 64, 77, 90, 103, 116, 129};
constexpr uint8_t COLS[] = {1, 25, 42, 59, 76, 93, 110, 127, 144};

class TFT
{
public:
  void setup();

  void displayConfig(Config::SharedPointer cfg);
  void displaySystemDebug();
  void displayMessage(String msg);

private:
  void __preDisplay()
  {
    m_tft.startWrite();
    m_tft.fillScreen(TFT_BLACK);
    m_tft.setTextDatum(BL_DATUM);
  }

  void __postDisplay()
  {
    m_tft.endWrite();
  }

  TFT_eSPI m_tft;
};
