// #include <iostream>

#include "file.h"

namespace psc
{
  namespace fs
  {

    static bool started = false;

    void begin()
    {
      if (!started)
      {
        if (!SPIFFS.begin(true, "/psc", 1))
        {
          // std::cout << "SPIFFS.begin() failed" << std::endl;
        }
        else
        {
          started = true;
        }
      }
      // std::cout << "FS used " << SPIFFS.usedBytes() << " bytes of " << SPIFFS.totalBytes() << " total" << std::endl;
    }

    ByteVector read(const String &path)
    {
      ByteVector data;
      if (!started)
      {
        // std::cout << "Cannot read file; FS not ready" << std::endl;
        return data;
      }
      File f = SPIFFS.open(path.c_str());
      if (!f || f.isDirectory())
      {
        // std::cout << "Error opening file for read" << std::endl;
        return data;
      }
      const auto len = f.size();
      data.resize(len);
      /* const size_t r = */ f.read(&data[0], len);
      // std::cout << "read " << r << " bytes from " << path.c_str() << std::endl;
      f.close();
      return data;
    }

    void write(const String &path, const ByteVector &data)
    {
      if (!started)
      {
        // std::cout << "Cannot read file; FS not ready" << std::endl;
        return;
      }
      File f = SPIFFS.open(path.c_str(), FILE_WRITE);
      if (!f)
      {
        // std::cout << "Error opening file for write" << std::endl;
        return;
      }
      /* const size_t w = */ f.write(&data[0], data.size());
      // std::cout << "wrote " << w << " bytes to " << path.c_str() << std::endl;
      f.close();
    }

    ByteVector toByteVector(const String &s)
    {
      ByteVector d;
      for (const auto &c : s)
      {
        d.push_back(c);
      }
      return d;
    }

    String toString(const ByteVector &data)
    {
      String s;
      for (const auto &c : s)
      {
        s += c;
      }
      return s;
    }

  } // namespace fs
} // namespace psc
