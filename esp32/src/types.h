#pragma once

#include <inttypes.h>
#include <memory>
#include <vector>

using ByteVector = std::vector<uint8_t>;

struct DACValue
{
  int16_t value = 0;
  bool gate = false;
  bool trig = false;
  uint8_t cc7value = 0;
  int16_t pitchbend = 0;
};

struct DACState
{
  DACValue dac[4];
};

class View
{
public:
  virtual void setup() = 0;
  virtual void update(const DACState &ss) = 0;
};

// TODO: could still be tuned?
constexpr uint32_t I2C_DELAY_US = 3000;
