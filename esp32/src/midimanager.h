#pragma once

// stdlib
#include <memory>

// platform libs
#include <Arduino.h>
#include <MIDI.h>

// misc-lib
#include <AY38910.h>
#include <signals.h>

// app parts
#include "config.h"
#include "ports.h"
#include "types.h"
#include "views.h"

// Serial MIDI interpreter settings
struct MidiSettings : public midi::DefaultSettings
{
  static const long BaudRate = SERIAL_SPEED;
  static const unsigned SysExMaxSize = 256;
};

// Serial MIDI interpreter
class MIDIManager
{
public:
  using SharedPointer = std::shared_ptr<MIDIManager>;

  using VoidSignal = Signal<>;
  using SysexSignal = Signal<const ByteVector &>;
  using MIDISignal = Signal<byte, byte, byte>;
  using PitchbendSignal = Signal<byte, int>;

  using SerialMidi = midi::SerialMIDI<HardwareSerial, MidiSettings>;

  MIDIManager(SerialPorts::SharedPointer ports)
      : s_allNotesOff(std::make_shared<VoidSignal>()),
        s_systemExclusive(std::make_shared<SysexSignal>()),
        s_noteOn(std::make_shared<MIDISignal>()),
        s_noteOff(std::make_shared<MIDISignal>()),
        s_controlChange(std::make_shared<MIDISignal>()),
        s_pitchBend(std::make_shared<PitchbendSignal>()),
        m_sm(ports->Console),
        m_midi(m_sm)
  {
  }

  void setup();
  void read();

private: // Signal members
  VoidSignal::SharedPointer s_allNotesOff;
  SysexSignal::SharedPointer s_systemExclusive;
  MIDISignal::SharedPointer s_noteOn;
  MIDISignal::SharedPointer s_noteOff;
  MIDISignal::SharedPointer s_controlChange;
  PitchbendSignal::SharedPointer s_pitchBend;

public: // Signal accessors
  VoidSignal::SharedPointer allNotesOff() const
  {
    return s_allNotesOff;
  }
  SysexSignal::SharedPointer systemExclusive() const
  {
    return s_systemExclusive;
  }
  MIDISignal::SharedPointer noteOn() const
  {
    return s_noteOn;
  }
  MIDISignal::SharedPointer noteOff() const
  {
    return s_noteOff;
  }
  MIDISignal::SharedPointer controlChange() const
  {
    return s_controlChange;
  }
  PitchbendSignal::SharedPointer pitchBend() const
  {
    return s_pitchBend;
  }

  void bufferSysex(const byte *array, unsigned size);

private: // Internal details
  SerialMidi m_sm;
  midi::MidiInterface<SerialMidi> m_midi;

  ByteVector m_sxbuf; // sysex message buffer
};
