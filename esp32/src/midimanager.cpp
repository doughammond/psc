#include "midimanager.h"

MIDIManager *__mmInstance = nullptr;

void MIDIManager::setup()
{
  __mmInstance = this;
  m_midi.begin(MIDI_CHANNEL_OMNI);
  m_midi.turnThruOff();

  // I don't like this; but the imperative read() for sysex doesn't work;
  // it will only ever give us the tail end of fragmented messages.
  // The callback implementation works "correctly", giving us all the data
  // although we still have to defragment it ourselves.
  // See: https://github.com/FortySevenEffects/arduino_midi_library/issues/222
  m_midi.setHandleSystemExclusive(
      [](byte *array, unsigned size)
      {
        if (__mmInstance != nullptr)
        {
          __mmInstance->bufferSysex(array, size);
        }
      });

  // We handle pitchbend via callback because MIDI library converts the int value for us
  m_midi.setHandlePitchBend(
      [](byte channel, int value)
      {
        if (__mmInstance != nullptr)
        {
          __mmInstance->pitchBend()->emit(channel, value);
        }
      });
}

void MIDIManager::read()
{
  while (m_midi.read())
  {
    switch (m_midi.getType())
    {
    case midi::MidiType::Stop:
      s_allNotesOff->emit();
      break;
    case midi::MidiType::SystemReset:
      s_allNotesOff->emit();
      break;
    case midi::MidiType::NoteOn:
      s_noteOn->emit(m_midi.getChannel(), m_midi.getData1(), m_midi.getData2());
      break;
    case midi::MidiType::NoteOff:
      s_noteOff->emit(m_midi.getChannel(), m_midi.getData1(), m_midi.getData2());
      break;
    case midi::MidiType::ControlChange:
      s_controlChange->emit(m_midi.getChannel(), m_midi.getData1(), m_midi.getData2());
      break;
    default:
      break;
    }
  }
}

void MIDIManager::bufferSysex(const byte *array, unsigned size)
{
  // Since MIDI SysEx messages can be fragmented, we'll buffer them here
  // and only emit complete messages back to the application

  for (unsigned i = 0; i < size; ++i)
  {
    m_sxbuf.push_back(array[i]);
  }

  auto buf_beg = m_sxbuf.cbegin();
  auto buf_end = m_sxbuf.cend();

  // Handle message continuation, whereby we get a {0xF0, 0xF7} pair in the data stream
  const auto continuation = std::adjacent_find(
      buf_beg, buf_end,
      [](const uint8_t &a, const uint8_t &b)
      {
        return a == 0xF0 && b == 0xF7;
      });
  if (continuation != buf_end)
  {
    // remove the continuation mark and reset begin/end iterators
    m_sxbuf.erase(continuation, continuation + 2);
    buf_beg = m_sxbuf.cbegin();
    buf_end = m_sxbuf.cend();
  }

  const auto mark_f0 = std::find(buf_beg, buf_end, 0xF0);
  const auto mark_f7 = std::find(buf_beg, buf_end, 0xF7);

  // Do we have a complete SysEx message?
  if (mark_f0 != buf_end && mark_f7 != buf_end)
  {
    // then send it; excluding 0xF0 and 0xF7
    const ByteVector message(mark_f0 + 1, mark_f7);
    s_systemExclusive->emit(message);

    // remove the entire sysex message, up to -0xF7 inclusive
    m_sxbuf.erase(buf_beg, mark_f7 + 1);
  }
}
