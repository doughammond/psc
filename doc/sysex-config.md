# SysEx Message Structure

## Envelope: Manufacturer, device and protocol identifiers

Refs:

- http://midi.teragonaudio.com/tech/midispec/sysex.htm
- http://midi.teragonaudio.com/tech/midispec/id.htm
- https://www.midi.org/specifications-old/item/manufacturer-id-numbers

```
    0xF0 <MANF> <BYTE>+ 0xF7
```

We need to use the first 3 bytes to identify the device manufacturer:

- `MANF`: `0x00` - using 'extended' IDs
- `BYTE[0]`: `0x60` - an unallocated ID block
- `BYTE[1]`: `0x00` - first number in unallocated block

The next byte will identify the device type:

- `BYTE[2]`: `0x00` - the PSC

The next byte will identify the protocol version:

- `BYTE[3]`: `0x00` - the first protocol (as per this spec)

Therefore, the overall SysEx message for this document will be in the form

```
    0xF0 0x00 0x60 0x00 0x00 0x00 <MESSAGE BYTES>[] 0xF7
```

(!) None of the `<MESSAGE BYTES>` can take the value `0xF7`, because that signals the end of the SysEx message.

## Protocol `0x00`

```
    ( <Config Type> <DAC Channel Mask> <PSG Channel Mask> <Value> ) +
```

The config strings can be repeated, in order to set multiple configuration values in the same SysEx message.

### Config Types:

- `0x00` - Channel
- `0x01` - Enable
- `0x02` - Mode
- `0x03` - Min
- `0x04` - Max
- `0x05` - CC7
- `0x06` - CC14

### DAC Channel Mask:

Using LSB for each of the DAC channels (4):

- `0b00000001` : Apply value to DAC output A
- `0b00000010` : Apply value to DAC output B
- `0b00000100` : Apply value to DAC output C
- `0b00001000` : Apply value to DAC output D

These values should be logically `OR`d together to specify which DAC channels the value applies to.

### PSG Channel Mask:

Using LSB for each of the PSG channels (4):

- `0b00000001` : Apply value to PSG Voice output A
- `0b00000010` : Apply value to PSG Voice output B
- `0b00000100` : Apply value to PSG Voice output C
- `0b00001000` : Apply value to PSG Noise output

These values should be logically `OR`d together to specify which PSG channels the value applies to.

### Config Values

#### Channel

There are 16 MIDI channels, so send the channel number as-is, in range `0x00` - `0x0F`.

#### Enable (for DAC channels)

Using LSB as a mask for `Value`, `Gate`, `Trigger`, `Gain2x`, `Pitch bend` respectively;

- `0b00000001` : Enable Value output
- `0b00000010` : Enable Gate output
- `0b00000100` : Enable Trigger output
- `0b00001000` : Enable 2x Gain
- `0b00010000` : Enable Pitch Bend (When in Note mode)

### Enable (for PSG channels)

Using LSB as a mask for `Enable`, `Mix A`, `Mix B`, `Mix C` respectively;

- `0b00000001` : Enable PSG Voice output
- `0b00000010` : Enable PSG Noise output output to Voice A
- `0b00000100` : Enable PSG Noise output output to Voice B
- `0b00001000` : Enable PSG Noise output output to Voice C

### Mode

**DAC and PSG Noise channels only; ignored for PSG Voice channels.**

- `0x00` : Note value
- `0x01` : Velocity value
- `0x02` : CC (7 bit)
- `0x03` :
  - For DAC: CC (14 bit) (interpreted as 12 bit using the 12 MSB)
  - For PSGN: Not supported, reverts to Note mode

### Min

**DAC Channels only.**

Set the MIDI note value which corresponds to minimum output, in range `0x00` - `0x7F`.

### Max

**DAC Channels only.**

Set the MIDI note value which corresponds to maximum output, in range `0x00` - `0x7F`.

## CC7

**DAC channels:**

Set the 7-bit CC number to use for the output value in CC7 mode, or the CC number to use for MSB in CC14 mode.
In range `0x00` - `0x7F`.

**PSG Noise channel:**

Set the 7-bit CC number to use for the output value, in range `0x00` - `0x7F`.

## CC14

**DAC channels only.**

Set the 7-bit CC number to use for the output value in CC7 mode, or the CC number to use for MSB in CC14 mode.
In range `0x00` - `0x7F`.

### Examples

These are examples of configuration strings, including the SysEx envelope.

**1. Set each output to sequential MIDI channels, 1-8:**

```
    0xF0 0x00 0x60 0x00 0x00 0x00
        0x00 0x01 0x00 0x00
        0x00 0x02 0x00 0x01
        0x00 0x04 0x00 0x02
        0x00 0x08 0x00 0x03
        0x00 0x00 0x01 0x04
        0x00 0x00 0x02 0x05
        0x00 0x00 0x04 0x06
        0x00 0x00 0x08 0x07
    0xF7
```

**2. Enable Value, Gate, Trigger for the DAC outputs A and B, Value only for DAC outputs C and D:**

```
    0xF0 0x00 0x60 0x00 0x00 0x00
        0x01 0x03 0x00 0x07
        0x01 0x0C 0x00 0x01
    0xF7
```

**3. Set all DAC outputs to CC7 mode, all PSG to Note mode:**

```
    0xF0 0x00 0x60 0x00 0x00 0x00
        0x02 0x0F 0x00 0x02
        0x02 0x00 0x0F 0x00
    0xF7
```

**4. Set the same Min (G1) and Max (D7) for all DAC outputs:**

```
    0xF0 0x00 0x60 0x00 0x00 0x00
        0x03 0x0F 0x00 0x1F
        0x04 0x0F 0x00 0x62
    0xF7
```

**5. Assign incrementing CC number pairs to DAC outputs and then set to CC14 mode:**

```
    0xF0 0x00 0x60 0x00 0x00 0x00
        0x05 0x01 0x00 0x14
        0x05 0x02 0x00 0x15
        0x05 0x04 0x00 0x16
        0x05 0x08 0x00 0x17
        0x06 0x01 0x00 0x32
        0x06 0x02 0x00 0x33
        0x06 0x04 0x00 0x34
        0x06 0x08 0x00 0x35
        0x02 0x0F 0x00 0x03
    0xF7
```
