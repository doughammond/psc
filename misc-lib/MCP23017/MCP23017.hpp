/***************************************************
  This is a library for the MCP23017 i2c port expander

  These displays use I2C to communicate, 2 pins are required to
  interface
  Adafruit invests time and resources providing this open source code,
  please support Adafruit and open-source hardware by purchasing
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.
  BSD license, all text above must be included in any redistribution

  Adapted D Hammond 2023 to templated C++ class

 ****************************************************/

#pragma once

#include <stdint.h>

inline bool bitRead(const uint8_t byte, const uint8_t bit);
inline void bitWrite(uint8_t &byte, const uint8_t bit, const uint8_t val);

template <class TwoWire, class GPIOMode>
class Adafruit_MCP23017
{
public:
    enum Registers
    {
        MCP23017_IODIRA = 0x00,
        MCP23017_IPOLA = 0x02,
        MCP23017_GPINTENA = 0x04,
        MCP23017_DEFVALA = 0x06,
        MCP23017_INTCONA = 0x08,
        MCP23017_IOCONA = 0x0A,
        MCP23017_GPPUA = 0x0C,
        MCP23017_INTFA = 0x0E,
        MCP23017_INTCAPA = 0x10,
        MCP23017_GPIOA = 0x12,
        MCP23017_OLATA = 0x14,

        MCP23017_IODIRB = 0x01,
        MCP23017_IPOLB = 0x03,
        MCP23017_GPINTENB = 0x05,
        MCP23017_DEFVALB = 0x07,
        MCP23017_INTCONB = 0x09,
        MCP23017_IOCONB = 0x0B,
        MCP23017_GPPUB = 0x0D,
        MCP23017_INTFB = 0x0F,
        MCP23017_INTCAPB = 0x11,
        MCP23017_GPIOB = 0x13,
        MCP23017_OLATB = 0x15,

        MCP23017_INT_ERR = 255,
    };

    void begin(TwoWire &w, uint8_t addr);

    void pinMode(uint8_t p, GPIOMode d);
    void digitalWrite(uint8_t p, uint8_t d);
    void pullUp(uint8_t p, uint8_t d);
    uint8_t digitalRead(uint8_t p);

    void writeGPIOAB(uint16_t);
    uint16_t readGPIOAB();
    uint8_t readGPIO(uint8_t b);

    void setupInterrupts(uint8_t mirroring, uint8_t open, uint8_t polarity);
    void setupInterruptPin(uint8_t p, uint8_t mode);
    uint8_t getLastInterruptPin();
    uint8_t getLastInterruptPinValue();

private:
    uint8_t i2caddr;

    uint8_t bitForPin(uint8_t pin);
    uint8_t regForPin(uint8_t pin, uint8_t portAaddr, uint8_t portBaddr);

    uint8_t readRegister(uint8_t addr);
    void writeRegister(uint8_t addr, uint8_t value);

    /**
     * Utility private method to update a register associated with a pin (whether port A/B)
     * reads its value, updates the particular bit, and writes its value.
     */
    void updateRegisterBit(uint8_t p, uint8_t pValue, uint8_t portAaddr, uint8_t portBaddr);

    TwoWire *m_wire;
};

// -------------------------------------------

/**
 * Bit number associated to a give Pin
 */
template <class TwoWire, class GPIOMode>
uint8_t Adafruit_MCP23017<TwoWire, GPIOMode>::bitForPin(uint8_t pin)
{
    return pin % 8;
}

/**
 * Register address, port dependent, for a given PIN
 */
template <class TwoWire, class GPIOMode>
uint8_t Adafruit_MCP23017<TwoWire, GPIOMode>::regForPin(uint8_t pin, uint8_t portAaddr, uint8_t portBaddr)
{
    return (pin < 8) ? portAaddr : portBaddr;
}

/**
 * Reads a given register
 */
template <class TwoWire, class GPIOMode>
uint8_t Adafruit_MCP23017<TwoWire, GPIOMode>::readRegister(uint8_t addr)
{
    // read the current GPINTEN
    m_wire->beginTransmission(i2caddr);
    m_wire->write(addr);
    m_wire->endTransmission();
    m_wire->requestFrom(i2caddr, 1);
    return m_wire->read();
}

/**
 * Writes a given register
 */
template <class TwoWire, class GPIOMode>
void Adafruit_MCP23017<TwoWire, GPIOMode>::writeRegister(uint8_t regAddr, uint8_t regValue)
{
    // Write the register
    m_wire->beginTransmission(i2caddr);
    m_wire->write(regAddr);
    m_wire->write(regValue);
    m_wire->endTransmission();
}

/**
 * Helper to update a single bit of an A/B register.
 * - Reads the current register value
 * - Writes the new register value
 */
template <class TwoWire, class GPIOMode>
void Adafruit_MCP23017<TwoWire, GPIOMode>::updateRegisterBit(uint8_t pin, uint8_t pValue, uint8_t portAaddr, uint8_t portBaddr)
{
    uint8_t regValue;
    uint8_t regAddr = regForPin(pin, portAaddr, portBaddr);
    uint8_t bit = bitForPin(pin);
    regValue = readRegister(regAddr);

    // set the value for the particular bit
    bitWrite(regValue, bit, pValue);

    writeRegister(regAddr, regValue);
}

////////////////////////////////////////////////////////////////////////////////

/**
 * Initializes the MCP23017 given its HW selected address, see datasheet for Address selection.
 */
template <class TwoWire, class GPIOMode>
void Adafruit_MCP23017<TwoWire, GPIOMode>::begin(TwoWire &w, uint8_t addr)
{
    m_wire = &w;
    i2caddr = addr;

    // m_wire->begin();

    // set defaults!
    // all inputs on port A and B
    writeRegister(MCP23017_IODIRA, 0xff);
    writeRegister(MCP23017_IODIRB, 0xff);
}

/**
 * Sets the pin mode to either INPUT or OUTPUT
 */
template <class TwoWire, class GPIOMode>
void Adafruit_MCP23017<TwoWire, GPIOMode>::pinMode(uint8_t p, GPIOMode d)
{
    updateRegisterBit(p, (d == GPIOMode::INPUT), MCP23017_IODIRA, MCP23017_IODIRB);
}

/**
 * Reads all 16 pins (port A and B) into a single 16 bits variable.
 */
template <class TwoWire, class GPIOMode>
uint16_t Adafruit_MCP23017<TwoWire, GPIOMode>::readGPIOAB()
{
    uint16_t ba = 0;
    uint8_t a;

    // read the current GPIO output latches
    m_wire->beginTransmission(i2caddr);
    m_wire->write(MCP23017_GPIOA);
    m_wire->endTransmission();

    m_wire->requestFrom(i2caddr, 2);
    a = m_wire->read();
    ba = m_wire->read();
    ba <<= 8;
    ba |= a;

    return ba;
}

/**
 * Read a single port, A or B, and return its current 8 bit value.
 * Parameter b should be 0 for GPIOA, and 1 for GPIOB.
 */
template <class TwoWire, class GPIOMode>
uint8_t Adafruit_MCP23017<TwoWire, GPIOMode>::readGPIO(uint8_t b)
{

    // read the current GPIO output latches
    m_wire->beginTransmission(i2caddr);
    if (b == 0)
        m_wire->write(MCP23017_GPIOA);
    else
    {
        m_wire->write(MCP23017_GPIOB);
    }
    m_wire->endTransmission();

    m_wire->requestFrom(i2caddr, 1);
    return m_wire->read();
}

/**
 * Writes all the pins in one go. This method is very useful if you are implementing a multiplexed matrix and want to get a decent refresh rate.
 */
template <class TwoWire, class GPIOMode>
void Adafruit_MCP23017<TwoWire, GPIOMode>::writeGPIOAB(uint16_t ba)
{
    m_wire->beginTransmission(i2caddr);
    m_wire->write(MCP23017_GPIOA);
    m_wire->write(ba & 0xFF);
    m_wire->write(ba >> 8);
    m_wire->endTransmission();
}

template <class TwoWire, class GPIOMode>
void Adafruit_MCP23017<TwoWire, GPIOMode>::digitalWrite(uint8_t pin, uint8_t d)
{
    uint8_t gpio;
    uint8_t bit = bitForPin(pin);

    // read the current GPIO output latches
    uint8_t regAddr = regForPin(pin, MCP23017_OLATA, MCP23017_OLATB);
    gpio = readRegister(regAddr);

    // set the pin and direction
    bitWrite(gpio, bit, d);

    // write the new GPIO
    regAddr = regForPin(pin, MCP23017_GPIOA, MCP23017_GPIOB);
    writeRegister(regAddr, gpio);
}

template <class TwoWire, class GPIOMode>
void Adafruit_MCP23017<TwoWire, GPIOMode>::pullUp(uint8_t p, uint8_t d)
{
    updateRegisterBit(p, d, MCP23017_GPPUA, MCP23017_GPPUB);
}

template <class TwoWire, class GPIOMode>
uint8_t Adafruit_MCP23017<TwoWire, GPIOMode>::digitalRead(uint8_t pin)
{
    uint8_t bit = bitForPin(pin);
    uint8_t regAddr = regForPin(pin, MCP23017_GPIOA, MCP23017_GPIOB);
    return (readRegister(regAddr) >> bit) & 0x1;
}

/**
 * Configures the interrupt system. both port A and B are assigned the same configuration.
 * Mirroring will OR both INTA and INTB pins.
 * Opendrain will set the INT pin to value or open drain.
 * polarity will set LOW or HIGH on interrupt.
 * Default values after Power On Reset are: (false,flase, LOW)
 * If you are connecting the INTA/B pin to arduino 2/3, you should configure the interupt handling as FALLING with
 * the default configuration.
 */
template <class TwoWire, class GPIOMode>
void Adafruit_MCP23017<TwoWire, GPIOMode>::setupInterrupts(uint8_t mirroring, uint8_t openDrain, uint8_t polarity)
{
    // configure the port A
    uint8_t ioconfValue = readRegister(MCP23017_IOCONA);
    bitWrite(ioconfValue, 6, mirroring);
    bitWrite(ioconfValue, 2, openDrain);
    bitWrite(ioconfValue, 1, polarity);
    writeRegister(MCP23017_IOCONA, ioconfValue);

    // Configure the port B
    ioconfValue = readRegister(MCP23017_IOCONB);
    bitWrite(ioconfValue, 6, mirroring);
    bitWrite(ioconfValue, 2, openDrain);
    bitWrite(ioconfValue, 1, polarity);
    writeRegister(MCP23017_IOCONB, ioconfValue);
}

/**
 * Set's up a pin for interrupt. uses arduino MODEs: CHANGE, FALLING, RISING.
 *
 * Note that the interrupt condition finishes when you read the information about the port / value
 * that caused the interrupt or you read the port itself. Check the datasheet can be confusing.
 *
 */
template <class TwoWire, class GPIOMode>
void Adafruit_MCP23017<TwoWire, GPIOMode>::setupInterruptPin(uint8_t pin, uint8_t mode)
{

    // set the pin interrupt control (0 means change, 1 means compare against given value);
    updateRegisterBit(pin, (mode != GPIOMode::CHANGE), MCP23017_INTCONA, MCP23017_INTCONB);
    // if the mode is not CHANGE, we need to set up a default value, different value triggers interrupt

    // In a RISING interrupt the default value is 0, interrupt is triggered when the pin goes to 1.
    // In a FALLING interrupt the default value is 1, interrupt is triggered when pin goes to 0.
    updateRegisterBit(pin, (mode == GPIOMode::FALLING), MCP23017_DEFVALA, MCP23017_DEFVALB);

    // enable the pin for interrupt
    updateRegisterBit(pin, GPIOMode::HIGH, MCP23017_GPINTENA, MCP23017_GPINTENB);
}

inline bool bitRead(const uint8_t byte, const uint8_t bit)
{
    return (byte & (1 << bit)) > 0;
}

inline void bitWrite(uint8_t &byte, const uint8_t bit, const uint8_t val)
{
    if (val)
    {
        byte |= 1 << bit;
    }
    else
    {
        byte &= ~(1 << bit);
    }
}

template <class TwoWire, class GPIOMode>
uint8_t Adafruit_MCP23017<TwoWire, GPIOMode>::getLastInterruptPin()
{
    uint8_t intf;

    // try port A
    intf = readRegister(MCP23017_INTFA);
    for (int i = 0; i < 8; i++)
        if (bitRead(intf, i))
            return i;

    // try port B
    intf = readRegister(MCP23017_INTFB);
    for (int i = 0; i < 8; i++)
        if (bitRead(intf, i))
            return i + 8;

    return MCP23017_INT_ERR;
}

template <class TwoWire, class GPIOMode>
uint8_t Adafruit_MCP23017<TwoWire, GPIOMode>::getLastInterruptPinValue()
{
    uint8_t intPin = getLastInterruptPin();
    if (intPin != MCP23017_INT_ERR)
    {
        uint8_t intcapreg = regForPin(intPin, MCP23017_INTCAPA, MCP23017_INTCAPB);
        uint8_t bit = bitForPin(intPin);
        return (readRegister(intcapreg) >> bit) & (0x01);
    }

    return MCP23017_INT_ERR;
}
