#include "AY38910.h"

AY38910::AY38910() : // lets start with an empty register set
                     m_frame{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}
{
  // set the GPIOs as all outputs;
  m_frame[AY38910::REG_IO_MIXER] |= IO_MIXER_PORT_A | IO_MIXER_PORT_B;
  // set all GPIO LOW
  m_frame[REG_IOA] = 0x0;
  m_frame[REG_IOB] = 0x0;

  // turn off the noise and tones
  allOff();
}

const AY38910::DataFrame &AY38910::getState() const
{
  return m_frame;
}

bool AY38910::allOff()
{
  // turn off the noise
  m_frame[AY38910::REG_IO_MIXER] |= IO_MIXER_NOISE_A | IO_MIXER_NOISE_B | IO_MIXER_NOISE_C;

  noteOff(0);
  noteOff(1);
  return noteOff(2);
}

bool AY38910::noteOn(const uint8_t channel, const uint8_t note, const uint8_t velocity)
{
  const AY38910::ToneRegisters tr = channelToRegisters(channel);
  // return same state for invalid channel
  if (tr.n_lsb == REG_INVALID || tr.n_msb == REG_INVALID || tr.n_lvl == REG_INVALID)
  {
    return false;
  }

  // we use a zero-velocity == note off convention;
  // but this is inefficient as calling noteOff causes
  // another ToneRegisters struct construction.
  if (velocity == 0)
  {
    return noteOff(channel);
  }

  // LSB / fine tune
  m_frame[tr.n_lsb] = ay38910notes[note] & 0xFF;
  // MSB / coarse tune
  m_frame[tr.n_msb] = (ay38910notes[note] >> 8) & 0xF;
  // velocity 7-bit to 4-bit
  m_frame[tr.n_lvl] = (velocity & 0x7F) >> 3;

  // channel enable is negative logic, clear the relevant bit
  m_frame[AY38910::REG_IO_MIXER] &= ~(tr.enable);

  // gate out is pos logic, set the relevant bit
  m_frame[REG_IOB] |= 1 << (3 + channel);

  return true;
}

bool AY38910::noteOff(const uint8_t channel)
{
  const AY38910::ToneRegisters tr = channelToRegisters(channel);
  // return same state for invalid channel
  if (tr.n_lsb == REG_INVALID || tr.n_msb == REG_INVALID || tr.n_lvl == REG_INVALID)
  {
    return false;
  }

  // LSB / fine tune
  // m_frame[tr.n_lsb] = 0;
  // MSB / coarse tune
  // m_frame[tr.n_msb] = 0;
  // velocity 7-bit to 4-bit
  m_frame[tr.n_lvl] = 0;

  // channel enable is negative logic, set the relevant bit
  m_frame[AY38910::REG_IO_MIXER] |= tr.enable;

  // gate out is pos logic, clear the relevant bit
  m_frame[REG_IOB] &= ~(1 << (3 + channel));

  return true;
}

bool AY38910::noiseOn(const uint8_t freq, const uint8_t output_mask)
{
  m_frame[AY38910::REG_FREQ_NOISE] = freq & 0x1F; // 5 bits

  m_frame[AY38910::REG_IO_MIXER] |= IO_MIXER_NOISE_A;
  m_frame[AY38910::REG_IO_MIXER] |= IO_MIXER_NOISE_B;
  m_frame[AY38910::REG_IO_MIXER] |= IO_MIXER_NOISE_C;

  if (output_mask & 0b001)
  {
    m_frame[AY38910::REG_IO_MIXER] &= ~(IO_MIXER_NOISE_A);
    if (m_frame[AY38910::REG_IO_MIXER] & IO_MIXER_A_EN)
    {
      m_frame[AY38910::REG_IO_MIXER] &= ~(IO_MIXER_A_EN);
    }
  }
  if (output_mask & 0b010)
  {
    m_frame[AY38910::REG_IO_MIXER] &= ~(IO_MIXER_NOISE_B);
    if (m_frame[AY38910::REG_IO_MIXER] & IO_MIXER_B_EN)
    {
      m_frame[AY38910::REG_IO_MIXER] &= ~(IO_MIXER_B_EN);
    }
  }
  if (output_mask & 0b100)
  {
    m_frame[AY38910::REG_IO_MIXER] &= ~(IO_MIXER_NOISE_C);
    if (m_frame[AY38910::REG_IO_MIXER] & IO_MIXER_C_EN)
    {
      m_frame[AY38910::REG_IO_MIXER] &= ~(IO_MIXER_C_EN);
    }
  }

  return true;
}

bool AY38910::writeGPIO(const uint16_t value)
{
  m_frame[AY38910::REG_IOA] = value & 0xFF;
  m_frame[AY38910::REG_IOB] = value >> 8;
  return true;
}

bool AY38910::setRegister(const AY38910::Registers reg, const uint8_t value)
{
  if (reg != AY38910::Registers::REG_INVALID)
  {
    m_frame[reg] = value;
  }
  return true;
}

AY38910::ToneRegisters AY38910::channelToRegisters(const uint8_t channel) const
{
  AY38910::ToneRegisters tr;
  switch (channel)
  {
  case 0:
    tr.n_lsb = AY38910::REG_FREQ_A_LO;
    tr.n_msb = AY38910::REG_FREQ_A_HI;
    tr.n_lvl = AY38910::REG_LVL_A;
    tr.enable = IO_MIXER_A_EN;
    break;
  case 1:
    tr.n_lsb = AY38910::REG_FREQ_B_LO;
    tr.n_msb = AY38910::REG_FREQ_B_HI;
    tr.n_lvl = AY38910::REG_LVL_B;
    tr.enable = IO_MIXER_B_EN;
    break;
  case 2:
    tr.n_lsb = AY38910::REG_FREQ_C_LO;
    tr.n_msb = AY38910::REG_FREQ_C_HI;
    tr.n_lvl = AY38910::REG_LVL_C;
    tr.enable = IO_MIXER_C_EN;
    break;
  default:
    break;
  }

  return tr;
}