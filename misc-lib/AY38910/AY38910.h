#pragma once

// STL
#include <memory>
#include <vector>

// lib
#include "ay38910notes.h"

#define IO_MIXER_PORT_A (0b01000000)
#define IO_MIXER_PORT_B (0b10000000)
#define IO_MIXER_NOISE_A (0b00001000)
#define IO_MIXER_NOISE_B (0b00010000)
#define IO_MIXER_NOISE_C (0b00100000)
#define IO_MIXER_A_EN (0b00000001)
#define IO_MIXER_B_EN (0b00000010)
#define IO_MIXER_C_EN (0b00000100)

/**
 * Manage the registers of AY-3-8910 device, and frame the
 * data for transmission over Serial line to a controller;
 * The receiving device blindly sets all registers in sequence
 * upon reception of a DataFrame.
 */
class AY38910
{
public:
  typedef std::shared_ptr<AY38910> SharedPointer;
  typedef std::vector<uint8_t> DataFrame;

  enum Registers
  {
    REG_FREQ_A_LO = 0,
    REG_FREQ_A_HI,
    REG_FREQ_B_LO,
    REG_FREQ_B_HI,
    REG_FREQ_C_LO,
    REG_FREQ_C_HI,

    REG_FREQ_NOISE,
    REG_IO_MIXER,

    REG_LVL_A,
    REG_LVL_B,
    REG_LVL_C,

    REG_FREQ_ENV_LO,
    REG_FREQ_ENV_HI,
    REG_ENV_SHAPE,

    REG_IOA,
    REG_IOB,

    REG_LAST,

    // placeholder for "not set"/unknown register
    REG_INVALID
  };

  AY38910();

  const DataFrame &getState() const;

  bool allOff();
  bool noteOn(const uint8_t channel, const uint8_t note, const uint8_t velocity);
  bool noteOff(const uint8_t channel);
  bool noiseOn(const uint8_t freq, const uint8_t output_mask);
  bool writeGPIO(const uint16_t value);
  bool setRegister(const AY38910::Registers reg, const uint8_t value);

private:
  struct ToneRegisters
  {
    AY38910::Registers n_lsb = AY38910::Registers::REG_INVALID;
    AY38910::Registers n_msb = AY38910::Registers::REG_INVALID;
    AY38910::Registers n_lvl = AY38910::Registers::REG_INVALID;
    uint8_t enable = 0;
  };

  AY38910::ToneRegisters channelToRegisters(const uint8_t channel) const;

private:
  DataFrame m_frame;
};
