#pragma once

// STL
#include <memory>

// libs
#include <signals.h>

typedef Signal<> TriggerSignal;

class ITimer
{
public:
  typedef std::shared_ptr<ITimer> SharedPointer;

  virtual void run(const uint16_t &interval) = 0;
  virtual void stop() = 0;
  virtual void single(const uint16_t &interval) = 0;

  virtual TriggerSignal::SharedPointer trigger() const = 0;

  virtual const bool armed() const = 0;
  virtual const bool running() const = 0;
};
