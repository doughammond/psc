/*******************************************************************
 *               AY-3-3910 Register writer for Arduino
 *                 (c) 2014 Manoel "Godzil" Trapier
 *
 * All the code is made by me apart from the the timer code that
 * was inspired from code found on the internet. I'm sorry, I can't
 * remmember where.
 **************************** Licence ******************************
 * This file is licenced under the licence:
 *                    WTFPL v2 Postal Card Edition:
 *
 *             DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *                    Version 2, December 2004
 *
 * Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>
 *
 * Everyone is permitted to copy and distribute verbatim or modified
 * copies of this license document, and changing it is allowed as long
 * as the name is changed.
 *
 *            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
 *   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 *
 *  0. You just DO WHAT THE FUCK YOU WANT TO.
 *  1. If you like this software you can send me a (virtual) postals
 *     card. Details bellow:
 *
 *             < godzil-nospambot at godzil dot net >
 *
 * If you want to send a real postal card, send me an email, I'll
 * give you my address. Of course remove the -nospambot from my
 * e-mail address.
 *
 * Adapted to ESP32 using I2S clock and bundled GPIO implementation
 * by Doug Hammond, 2023.
 *
 ******************************************************************/

#pragma once

// OS
#include <driver/dedic_gpio.h>

// Libs
#include <AY38910.h>

// App
#include "psc-board.h"

namespace psc::ay
{

    void setup();
    void writeRegisters(const AY38910::DataFrame frm);

} // namespace psc::ay
