#include <esp_log.h>

#include "psc-usb.h"
#include "psc-midi.h"
#include "psc-tft.h"
#include "psc-config.h"
#include "psc-ay.h"
#include "psc-io.hpp"
#include "psc-dac.h"
#include "psc-gatetrig.h"

static const char *TAG = "main";

// ----------

namespace psc::state
{
    static psc::config::Config cfg;
    static psc::dac::DACState dac;
} // namespace psc::state

namespace psc::ay
{
    static AY38910 message;
} // namespace psc::ay

namespace psc::hw
{
    // Busses
    static psc::io::ArduinoTwoWire wire(DAC_SCL, DAC_SDA, 400000);
    static psc::io::ArduinoGPIO gpio;
} // namespace psc::hw

// ----------

void handleAllNotesOff()
{
    for (int num = 0; num < 4; ++num)
    {
        psc::state::dac[num].gate = false;
        psc::state::dac[num].trig = false;
    }
    psc::dac::update(psc::state::dac);

    psc::ay::message.allOff();
    psc::ay::writeRegisters(psc::ay::message.getState());

    psc::gatetrig::update(psc::state::dac);
}

void handleSystemExclusive(const ByteVector &message)
{
    const auto sm = message.cbegin();
    const auto em = message.cend();

    // TODO: move all this to Config?
    // inspect the message header
    // [Extended manufacturer, Manufacturer byte1, Manufacturer byte2, device ID, protocol ID];
    const ByteVector href{0x00, 0x60, 0x00, 0x00, 0x00};
    // TODO: wrap this condition in Config.isProtocol0(message);
    if (std::equal(sm, sm + 5, href.cbegin()))
    {
        // chop out the data from the sysex message and consume it
        const ByteVector data(sm + 5, em);
        psc::state::cfg.parseProtocol0(data);
        psc::dac::setGain2x(
            psc::state::cfg.dac[0].enable.gain2x,
            psc::state::cfg.dac[1].enable.gain2x,
            psc::state::cfg.dac[2].enable.gain2x,
            psc::state::cfg.dac[3].enable.gain2x);
        psc::state::cfg.save("/default.conf");
        psc::tft::displayConfig(psc::state::cfg);
    }
}

void handleNoteOn(byte channel, byte note, byte velocity)
{
    bool dacupdate = false;
    for (int num = 0; num < 4; ++num)
    {
        if (psc::state::cfg.dac[num].channel == channel)
        {
            if (psc::state::cfg.dac[num].mode == psc::config::DACMode::NOTE)
            {
                psc::state::dac[num].gate = psc::state::cfg.dac[num].enable.gate;
                psc::state::dac[num].trig = psc::state::cfg.dac[num].enable.trigger;
                if (psc::state::cfg.dac[num].enable.value)
                {
                    const float rn = psc::state::cfg.dac[num].max - psc::state::cfg.dac[num].min;
                    const float sv = (note - psc::state::cfg.dac[num].min) / rn;
                    psc::state::dac[num].value = sv * 0xFFF;
                }
                dacupdate = true;
            }
            if (psc::state::cfg.dac[num].mode == psc::config::DACMode::VELOCITY)
            {
                psc::state::dac[num].gate = psc::state::cfg.dac[num].enable.gate;
                psc::state::dac[num].trig = psc::state::cfg.dac[num].enable.trigger;
                if (psc::state::cfg.dac[num].enable.value)
                {
                    const float rn = psc::state::cfg.dac[num].max - psc::state::cfg.dac[num].min;
                    const float sv = (velocity - psc::state::cfg.dac[num].min) / rn;
                    psc::state::dac[num].value = sv * 0xFFF;
                }
                dacupdate = true;
            }
        }
        else
        {
            psc::state::dac[num].trig = false;
            dacupdate = true;
        }
    }

    bool psgupdate = false;
    for (int num = 0; num < 3; ++num)
    {
        if (psc::state::cfg.psgv[num].enable && psc::state::cfg.psgv[num].channel == channel)
        {
            psgupdate |= psc::ay::message.noteOn(num, note, velocity);
        }
    }
    if (psc::state::cfg.psgn[0].channel == channel && (psc::state::cfg.psgn[0].mixmask & 0x1))
    {
        if (psc::state::cfg.psgn[0].mode == psc::config::PSGNMode::NOTE)
        {
            psgupdate |= psc::ay::message.noiseOn(note >> 2, psc::state::cfg.psgn[0].mixmask >> 1);
        }
        if (psc::state::cfg.psgn[0].mode == psc::config::PSGNMode::VELOCITY)
        {
            psgupdate |= psc::ay::message.noiseOn(velocity >> 2, psc::state::cfg.psgn[0].mixmask >> 1);
        }
    }

    if (dacupdate)
    {
        psc::dac::update(psc::state::dac);
        psc::gatetrig::update(psc::state::dac);
    }
    if (psgupdate)
    {
        psc::ay::writeRegisters(psc::ay::message.getState());
    }
}

void handleNoteOff(byte channel, byte note, byte velocity)
{
    bool dacupdate = false;
    for (int num = 0; num < 4; ++num)
    {
        if (psc::state::cfg.dac[num].channel == channel)
        {
            psc::state::dac[num].trig = false;
            psc::state::dac[num].gate = false;
            dacupdate = true;
        }
    }

    bool psgupdate = false;
    for (int num = 0; num < 3; ++num)
    {
        if (psc::state::cfg.psgv[num].enable && psc::state::cfg.psgv[num].channel == channel)
        {
            psgupdate |= psc::ay::message.noteOff(num);
        }
    }

    if (psc::state::cfg.psgn[0].channel == channel)
    {
        if (psc::state::cfg.psgn[0].mode == psc::config::PSGNMode::NOTE)
        {
            psgupdate |= psc::ay::message.noiseOn(note >> 2, 0);
        }
        if (psc::state::cfg.psgn[0].mode == psc::config::PSGNMode::VELOCITY)
        {
            psgupdate |= psc::ay::message.noiseOn(velocity >> 2, 0);
        }
    }

    if (dacupdate)
    {
        psc::dac::update(psc::state::dac);
        psc::gatetrig::update(psc::state::dac);
    }
    if (psgupdate)
    {
        psc::ay::writeRegisters(psc::ay::message.getState());
    }
}

void handleControlChange(byte channel, byte number, byte value)
{
    bool dacupdate = false;
    for (int num = 0; num < 4; ++num)
    {
        const bool isEnabled = psc::state::cfg.dac[num].channel == channel && psc::state::cfg.dac[num].enable.value;
        const bool isCCMode = psc::state::cfg.dac[num].mode == psc::config::DACMode::CC7 || psc::state::cfg.dac[num].mode == psc::config::DACMode::CC14;

        if (isEnabled && isCCMode)
        {
            if (psc::state::cfg.dac[num].CC[0] == number)
            {
                // Stash this value in case CC14 needs it
                psc::state::dac[num].cc7value = value & 0x7F;

                if (psc::state::cfg.dac[num].mode == psc::config::DACMode::CC7)
                {
                    const float rn = psc::state::cfg.dac[num].max - psc::state::cfg.dac[num].min;
                    const float sv = (value - psc::state::cfg.dac[num].min) / rn;
                    psc::state::dac[num].trig = true;
                    psc::state::dac[num].value = sv * 0xFFF;
                    dacupdate = true;
                }
            }
            if (psc::state::cfg.dac[num].mode == psc::config::DACMode::CC14 && psc::state::cfg.dac[num].CC[1] == number)
            {
                psc::state::dac[num].trig = true;
                psc::state::dac[num].value = ((psc::state::dac[num].cc7value << 7) | (value & 0x7F)) >> 2;
                dacupdate = true;
            }
        }
    }

    bool psgupdate = false;
    if (psc::state::cfg.psgn[0].channel == channel && (psc::state::cfg.psgn[0].mixmask & 0x1))
    {
        if (psc::state::cfg.psgn[0].mode == psc::config::PSGNMode::CC7 && psc::state::cfg.psgn[0].CC[0] == number)
        {
            psgupdate |= psc::ay::message.noiseOn(value >> 2, psc::state::cfg.psgn[0].mixmask >> 1);
        }
    }

    if (dacupdate)
    {
        psc::dac::update(psc::state::dac);
        psc::gatetrig::update(psc::state::dac);
    }
    if (psgupdate)
    {
        psc::ay::writeRegisters(psc::ay::message.getState());
    }
}

void handlePitchBend(byte channel, int value)
{
    bool dacupdate = false;
    for (int num = 0; num < 4; ++num)
    {
        const bool isEnabled = psc::state::cfg.dac[num].channel == channel && psc::state::cfg.dac[num].enable.value;
        const bool isNoteMode = psc::state::cfg.dac[num].mode == psc::config::DACMode::NOTE;

        if (isEnabled && isNoteMode)
        {
            if (psc::state::cfg.dac[num].enable.pitchbend)
            {
                psc::state::dac[num].pitchbend = value;
                dacupdate = true;
            }
            else if (psc::state::dac[num].pitchbend != 0)
            {
                psc::state::dac[num].pitchbend = 0;
                dacupdate = true;
            }
        }
        else if (psc::state::dac[num].pitchbend != 0)
        {
            psc::state::dac[num].pitchbend = 0;
            dacupdate = true;
        }
    }

    if (dacupdate)
    {
        psc::dac::update(psc::state::dac);
        psc::gatetrig::update(psc::state::dac);
    }

    // TODO: handle PB on AY?
}

extern "C" void app_main(void)
{
    psc::usb::setup();
    psc::midi::setup();
    psc::tft::setup();
    psc::config::setup();
    psc::ay::setup();
    psc::dac::setup(psc::hw::wire, psc::hw::gpio, DAC_NLDAC);

    psc::gatetrig::setup(psc::hw::wire);

    psc::state::cfg.load("default");

    psc::midi::allNotesOff().connect(handleAllNotesOff);
    psc::midi::systemExclusive().connect(handleSystemExclusive);
    psc::midi::noteOn().connect(handleNoteOn);
    psc::midi::noteOff().connect(handleNoteOff);
    psc::midi::controlChange().connect(handleControlChange);
    psc::midi::pitchBend().connect(handlePitchBend);

    psc::tft::displayConfig(psc::state::cfg);

    ESP_LOGI(TAG, "Setup complete");
}
