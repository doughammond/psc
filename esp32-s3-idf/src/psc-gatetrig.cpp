#include <unistd.h>

#include "psc-gatetrig.h"

namespace psc::gatetrig
{
    // TODO: replace with a ws2812 ?
    constexpr uint16_t LEDR = 0b0000000100000000;
    constexpr uint16_t LEDG = 0b0000001000000000;
    constexpr uint16_t LEDB = 0b0000010000000000;
    constexpr uint16_t LEDW = 0b0000011100000000;

    constexpr uint16_t MTRG = 0b1000000000000000;

    uint16_t quiescestate;

    static Adafruit_MCP23017<psc::io::ArduinoTwoWire, psc::io::ArduinoGPIO::MODE> ioexp;

    void setup(psc::io::ArduinoTwoWire &wire)
    {
        ioexp.begin(wire, MCP23017_ADDR);

        // gates
        ioexp.pinMode(0, psc::io::ArduinoGPIO::MODE::OUTPUT);
        ioexp.pinMode(1, psc::io::ArduinoGPIO::MODE::OUTPUT);
        ioexp.pinMode(2, psc::io::ArduinoGPIO::MODE::OUTPUT);
        ioexp.pinMode(3, psc::io::ArduinoGPIO::MODE::OUTPUT);

        // trigs
        ioexp.pinMode(4, psc::io::ArduinoGPIO::MODE::OUTPUT);
        ioexp.pinMode(5, psc::io::ArduinoGPIO::MODE::OUTPUT);
        ioexp.pinMode(6, psc::io::ArduinoGPIO::MODE::OUTPUT);
        ioexp.pinMode(7, psc::io::ArduinoGPIO::MODE::OUTPUT);

        // // rgb led : TODO: replace with a ws2812 ?
        // ioexp.pinMode(8, psc::io::ArduinoGPIO::MODE::OUTPUT);
        // ioexp.pinMode(9, psc::io::ArduinoGPIO::MODE::OUTPUT);
        // ioexp.pinMode(10, psc::io::ArduinoGPIO::MODE::OUTPUT);

        // master trig
        ioexp.pinMode(11, psc::io::ArduinoGPIO::MODE::OUTPUT);

        // connect the LED timer trigger to GPIO state set
        // m_tmr_led.trigger()->connect(
        //     [=]()
        //     { ioexp.writeGPIOAB(quiescestate); });
    }

    void update(const psc::dac::DACState &ss, const bool _setLED)
    {
        // channel gates on GPIO A [0:3] & channel trigs on GPIO A [4:7]
        const uint16_t ga = (ss[3].gate << 3) | (ss[2].gate << 2) | (ss[1].gate << 1) | (ss[0].gate);
        const uint16_t ta = (ss[3].trig << 7) | (ss[2].trig << 6) | (ss[1].trig << 5) | (ss[0].trig << 4);

        // if (setLED)
        // {
        //   // // RGBLED on GPIO B [0:2]; white on 1 / green on 4's / red on others
        //   // const uint16_t gb = ss.step == 0 ? (LEDW) : ((ss.step % 4) == 0 ? (LEDG) : (LEDR));
        //   // // master trig on GPIO B [7]
        //   // const uint16_t tb = MTRG;

        //   // // all gates, trigs and LED on initally
        //   // ioexp.writeGPIOAB(ga | ta | gb | tb);
        //   // usleep(psc::io::I2C_DELAY_US);
        //   // // trigs turn off immediately, leaves gates and LED active
        //   // ioexp.writeGPIOAB(ga | gb);
        //   // usleep(psc::io::I2C_DELAY_US);
        //   // // std::cout << "StepView setLED " << (ga | gb) << std::endl;

        //   // // RGBLED turn off in a short while, leaves gates active
        //   // quiescestate = ga;
        //   // m_tmr_led.single(25);
        // }
        // else
        {
            // master trig on GPIO B [7]
            const uint16_t tb = MTRG;

            // all gates, trigs on initally
            ioexp.writeGPIOAB(ga | ta | tb);
            usleep(psc::io::I2C_DELAY_US);

            // trigs turn off immediately, leaves gates active
            ioexp.writeGPIOAB(ga);
            usleep(psc::io::I2C_DELAY_US);
            // std::cout << "StepView !setLED " << (ga) << std::endl;

            // leave gates active
            quiescestate = ga;
        }
    }

    void update(const psc::dac::DACState &ss)
    {
        update(ss, false);
    }

} // namespace psc::gatetrig
