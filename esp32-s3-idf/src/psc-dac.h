#pragma once

#include <stdint.h>

#include "psc-io.hpp"
#include "psc-board.h"

namespace psc::dac
{
    struct DACValue
    {
        int16_t value = 0;
        bool gate = false;
        bool trig = false;
        uint8_t cc7value = 0;
        int16_t pitchbend = 0;
    };

    using DACState = DACValue[4];

    void setup(psc::io::ArduinoTwoWire &wire, psc::io::ArduinoGPIO &gpio, const uint8_t &nld);

    void setGain2x(const bool a, const bool b, const bool c, const bool d);
    void update(const DACState &ss);

} // namespace psc::dac
