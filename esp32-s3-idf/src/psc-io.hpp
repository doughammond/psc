#pragma once

#include <deque>

// #include <esp_log.h>
#include <driver/i2c.h>
#include <driver/gpio.h>

namespace psc::io
{
    // static const char *TAG = "io";

    // TODO: could still be tuned?
    constexpr uint32_t I2C_DELAY_US = 3000;

    // Emulate Arduino I2C interface
    class ArduinoTwoWire
    {
    public:
        ArduinoTwoWire(const int scl, const int sda, uint32_t freq)
        {
            i2c_port_t i2c_master_port = I2C_NUM_0;

            i2c_config_t conf = {
                .mode = I2C_MODE_MASTER,
                .sda_io_num = sda,
                .scl_io_num = scl,
                .sda_pullup_en = GPIO_PULLUP_ENABLE,
                .scl_pullup_en = GPIO_PULLUP_ENABLE,
                .master = {
                    .clk_speed = freq,
                },
                .clk_flags = I2C_SCLK_SRC_FLAG_FOR_NOMAL,
            };

            i2c_param_config(i2c_master_port, &conf);
            i2c_driver_install(i2c_master_port, conf.mode, 0, 0, 0);
        }

        void beginTransmission(const uint8_t addr)
        {
            m_cmd_handle = i2c_cmd_link_create();
            i2c_master_start(m_cmd_handle);
            i2c_master_write_byte(m_cmd_handle, (addr << 1) | I2C_MASTER_WRITE, true);
            // ESP_LOGI(TAG, "TwoWire write start: 0x%x", addr);
        }

        void write(const uint8_t data)
        {
            i2c_master_write_byte(m_cmd_handle, data, true);
            // ESP_LOGI(TAG, "TwoWire write byte: 0x%x", data);
        }

        uint8_t endTransmission()
        {
            i2c_master_stop(m_cmd_handle);
            /* const auto res = */ i2c_master_cmd_begin(I2C_NUM_0, m_cmd_handle, 1000 / portTICK_PERIOD_MS);
            // ESP_LOGI(TAG, "TwoWire write end: 0x%x", res);
            i2c_cmd_link_delete(m_cmd_handle);
            return 0;
        }

        void requestFrom(int addr, size_t num)
        {
            if (num > 128)
            {
                return;
            }

            const auto res = i2c_master_read_from_device(I2C_NUM_0, addr, m_rawinbuf, num, 1000 / portTICK_PERIOD_MS);
            // ESP_LOGI(TAG, "TwoWire read: 0x%x %d 0x%x", addr, num, res);
            if (res != ESP_OK)
            {
                return;
            }

            for (size_t i = 0; i < num; ++i)
            {
                m_inbuf.push_back(m_rawinbuf[i]);
            }
            // ESP_LOGI(TAG, "TwoWire read len %d", num);
        }

        int available()
        {
            const auto av = m_inbuf.size();
            // ESP_LOGI(TAG, "TwoWire available %d", av);
            return av;
        }

        uint8_t read()
        {
            // const auto av = m_inbuf.size();
            // ESP_LOGI(TAG, "TwoWire read available %d", av);
            const auto val = m_inbuf.front();
            m_inbuf.pop_front();
            return val;
        }

    private:
        i2c_cmd_handle_t m_cmd_handle;
        uint8_t m_rawinbuf[128];
        std::deque<uint8_t> m_inbuf; // a simple ring buffer might be better here?
    };

    // Emulate Arduino GPIO interface
    class ArduinoGPIO
    {
    public:
        enum class MODE
        {
            INPUT = GPIO_MODE_INPUT,
            OUTPUT = GPIO_MODE_OUTPUT,

            CHANGE,
            FALLING,

            HIGH,
            LOW
        };

        void pinMode(int pin, MODE mode)
        {
            gpio_config_t conf = {
                .pin_bit_mask = 1ULL << pin,
                .mode = (gpio_mode_t)mode,
                .pull_up_en = GPIO_PULLUP_DISABLE,
                .pull_down_en = GPIO_PULLDOWN_DISABLE,
                .intr_type = GPIO_INTR_DISABLE,
            };
            gpio_config(&conf);
        }

        void digitalWrite(int pin, bool val)
        {
            gpio_set_level((gpio_num_t)pin, val);
        }
    };

} // namespace psc::io
