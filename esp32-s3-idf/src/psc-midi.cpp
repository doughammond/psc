#include "psc-midi.h"

namespace psc::midi
{
    static TUSBTransport transport;
    static ::midi::MidiInterface<TUSBTransport, ::midi::DefaultSettings, ESPIDFPlatform> midi(transport);
    static ByteVector sxbuf; // sysex message buffer

    static VoidSignal s_allNotesOff;
    static SysexSignal s_systemExclusive;
    static MIDISignal s_noteOn;
    static MIDISignal s_noteOff;
    static MIDISignal s_controlChange;
    static PitchbendSignal s_pitchBend;

    VoidSignal &allNotesOff()
    {
        return s_allNotesOff;
    }

    SysexSignal &systemExclusive()
    {
        return s_systemExclusive;
    }

    MIDISignal &noteOn()
    {
        return s_noteOn;
    }

    MIDISignal &noteOff()
    {
        return s_noteOff;
    }

    MIDISignal &controlChange()
    {
        return s_controlChange;
    }

    PitchbendSignal &pitchBend()
    {
        return s_pitchBend;
    }

    void bufferSysex(const byte *array, unsigned size)
    {
        // Since MIDI SysEx messages can be fragmented, we'll buffer them here
        // and only emit complete messages back to the application

        for (unsigned i = 0; i < size; ++i)
        {
            sxbuf.push_back(array[i]);
        }

        auto buf_beg = sxbuf.cbegin();
        auto buf_end = sxbuf.cend();

        // Handle message continuation, whereby we get a {0xF0, 0xF7} pair in the data stream
        const auto continuation = std::adjacent_find(
            buf_beg, buf_end,
            [](const uint8_t &a, const uint8_t &b)
            {
                return a == 0xF0 && b == 0xF7;
            });
        if (continuation != buf_end)
        {
            // remove the continuation mark and reset begin/end iterators
            sxbuf.erase(continuation, continuation + 2);
            buf_beg = sxbuf.cbegin();
            buf_end = sxbuf.cend();
        }

        const auto mark_f0 = std::find(buf_beg, buf_end, 0xF0);
        const auto mark_f7 = std::find(buf_beg, buf_end, 0xF7);

        // Do we have a complete SysEx message?
        if (mark_f0 != buf_end && mark_f7 != buf_end)
        {
            // then send it; excluding 0xF0 and 0xF7
            const ByteVector message(mark_f0 + 1, mark_f7);
            s_systemExclusive.emit(message);

            // remove the entire sysex message, up to -0xF7 inclusive
            sxbuf.erase(buf_beg, mark_f7 + 1);
        }
    }

    void midi_task_read(void *arg)
    {
        for (;;)
        {
            vTaskDelay(1);
            while (midi.read())
            {
                switch (midi.getType())
                {
                case ::midi::MidiType::Stop:
                    // ESP_LOGI(TAG, "stop");
                    s_allNotesOff.emit();
                    break;
                case ::midi::MidiType::SystemReset:
                    // ESP_LOGI(TAG, "reset");
                    s_allNotesOff.emit();
                    break;
                case ::midi::MidiType::NoteOn:
                    // ESP_LOGI(TAG, "note on");
                    s_noteOn.emit(midi.getChannel(), midi.getData1(), midi.getData2());
                    break;
                case ::midi::MidiType::NoteOff:
                    // ESP_LOGI(TAG, "note off");
                    s_noteOff.emit(midi.getChannel(), midi.getData1(), midi.getData2());
                    break;
                case ::midi::MidiType::ControlChange:
                    // ESP_LOGI(TAG, "cc");
                    s_controlChange.emit(midi.getChannel(), midi.getData1(), midi.getData2());
                    break;
                default:
                    break;
                }
            }
        }
    }

    void setup()
    {
        midi.begin(MIDI_CHANNEL_OMNI);
        midi.turnThruOff();

        // I don't like this; but the imperative read() for sysex doesn't work;
        // it will only ever give us the tail end of fragmented messages.
        // The callback implementation works "correctly", giving us all the data
        // although we still have to defragment it ourselves.
        // See: https://github.com/FortySevenEffects/arduino_midi_library/issues/222
        midi.setHandleSystemExclusive(
            [](byte *array, unsigned size)
            {
                // ESP_LOGI(TAG, "sysex");
                bufferSysex(array, size);
            });

        // We handle pitchbend via callback because MIDI library converts the int value for us
        midi.setHandlePitchBend(
            [](byte channel, int value)
            {
                // ESP_LOGI(TAG, "pitchBend");
                s_pitchBend.emit(channel, value);
            });

        // TODO: revise stack size down?
        // ref: https://www.esp32.com/viewtopic.php?t=3692
        xTaskCreate(midi_task_read, "midi_task_read", 8 * 1024, NULL, 5, NULL);
    }

} // namespace psc::midi
