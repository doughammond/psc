#pragma once

#include <vector>

// #include <esp_log.h>
#include <esp_timer.h>

#include <FreeRTOS.h>
#include <freertos/task.h>

#include <MIDI.h>

#include <signals.h>

#include "psc-usb.h"

namespace psc::midi
{
    // static const char *TAG = "midi";

    using ByteVector = std::vector<uint8_t>;

    class TUSBTransport
    {
    public:
        void begin() {}

        unsigned available()
        {
            return tud_midi_available();
        }

        byte read()
        {
            uint8_t ch;
            return tud_midi_stream_read(&ch, 1) ? (int)ch : (-1);
        }

        bool beginTransmission(int val)
        {
            // TODO
            return false;
        }

        void write(int val)
        {
            // TODO
        }

        void endTransmission()
        {
            // TODO
        }

    public:
        bool thruActivated = false;
    };

    class ESPIDFPlatform
    {
    public:
        static unsigned long now()
        {
            return esp_timer_get_time() / 1000;
        }
    };

    using VoidSignal = Signal<>;
    using SysexSignal = Signal<const ByteVector &>;
    using MIDISignal = Signal<byte, byte, byte>;
    using PitchbendSignal = Signal<byte, int>;

    VoidSignal &allNotesOff();
    SysexSignal &systemExclusive();
    MIDISignal &noteOn();
    MIDISignal &noteOff();
    MIDISignal &controlChange();
    PitchbendSignal &pitchBend();

    void setup();

} // namespace psc::midi
