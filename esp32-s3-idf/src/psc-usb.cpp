#include "psc-usb.h"

namespace psc::usb
{
    /** TinyUSB descriptors **/

    // Interface counter
    enum interface_count
    {
#if CFG_TUD_MIDI
        ITF_NUM_MIDI = 0,
        ITF_NUM_MIDI_STREAMING,
#endif
        ITF_COUNT
    };

    // USB Endpoint numbers
    enum usb_endpoints
    {
        // Available USB Endpoints: 5 IN/OUT EPs and 1 IN EP
        EP_EMPTY = 0,
#if CFG_TUD_MIDI
        EPNUM_MIDI,
#endif
    };

#define TUSB_DESCRIPTOR_TOTAL_LEN (TUD_CONFIG_DESC_LEN + CFG_TUD_MIDI * TUD_MIDI_DESC_LEN)

    /**
     * @brief String descriptor
     */
    static const char *s_str_desc[5] = {
        // array of pointer to string descriptors
        (char[]){0x09, 0x04}, // 0: is supported language is English (0x0409)
        "lon.dev",            // 1: Manufacturer
        "psc",                // 2: Product
        "123456",             // 3: Serials, should use chip ID
        "psc",                // 4: MIDI
    };

    /**
     * @brief Configuration descriptor
     *
     * This is a simple configuration descriptor that defines 1 configuration and a MIDI interface
     */
    static const uint8_t s_midi_cfg_desc[] = {
        // Configuration number, interface count, string index, total length, attribute, power in mA
        TUD_CONFIG_DESCRIPTOR(1, ITF_COUNT, 0, TUSB_DESCRIPTOR_TOTAL_LEN, 0, 100),

        // Interface number, string index, EP Out & EP In address, EP size
        TUD_MIDI_DESCRIPTOR(ITF_NUM_MIDI, 4, EPNUM_MIDI, (0x80 | EPNUM_MIDI), 64),
    };

    static const char *TAG = "psg-usb";

    void setup()
    {
        ESP_LOGI(TAG, "USB initialization");

        tinyusb_config_t const tusb_cfg = {
            .device_descriptor = NULL, // If device_descriptor is NULL, tinyusb_driver_install() will use Kconfig
            .string_descriptor = s_str_desc,
            .string_descriptor_count = sizeof(s_str_desc) / sizeof(s_str_desc[0]),
            .external_phy = false,
            .configuration_descriptor = s_midi_cfg_desc,
            .self_powered = false,
            .vbus_monitor_io = -1,
        };
        ESP_ERROR_CHECK(tinyusb_driver_install(&tusb_cfg));

        ESP_LOGI(TAG, "USB initialization DONE");
    }
} // namespace psc::usb
