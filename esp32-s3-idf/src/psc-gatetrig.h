#pragma once

#include <MCP23017.hpp>

#include "psc-board.h"
#include "psc-io.hpp"
#include "psc-dac.h"

namespace psc::gatetrig
{
    void setup(psc::io::ArduinoTwoWire &wire);

    void update(const psc::dac::DACState &ss);

} // namespace psc::gatetrig