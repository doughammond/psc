#pragma once

#include <esp_log.h>
#include <tinyusb.h>

namespace psc::usb
{

    void setup();

} // namespace psc::usb
