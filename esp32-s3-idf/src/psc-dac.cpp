#include <algorithm>

#include <unistd.h>

#include <MCP4728.h>

#include "psc-dac.h"

namespace psc::dac
{
    using DacDevice = MCP4728<psc::io::ArduinoTwoWire, psc::io::ArduinoGPIO>;
    static DacDevice dacDevice;

    void setup(psc::io::ArduinoTwoWire &wire, psc::io::ArduinoGPIO &gpio, const uint8_t &nld)
    {
        // DAC setup
        dacDevice.attach(wire, gpio, nld);
        dacDevice.selectVref(DacDevice::VREF::INTERNAL_2_8V, DacDevice::VREF::INTERNAL_2_8V, DacDevice::VREF::INTERNAL_2_8V, DacDevice::VREF::INTERNAL_2_8V);
        dacDevice.selectPowerDown(DacDevice::PWR_DOWN::GND_500KOHM, DacDevice::PWR_DOWN::GND_500KOHM, DacDevice::PWR_DOWN::GND_500KOHM, DacDevice::PWR_DOWN::GND_500KOHM);
        dacDevice.selectGain(DacDevice::GAIN::X1, DacDevice::GAIN::X1, DacDevice::GAIN::X1, DacDevice::GAIN::X1);
        dacDevice.analogWrite(0, 0, 0, 0);
        dacDevice.enable(true);
        dacDevice.readRegisters();

        usleep(100000);
    }

    void setGain2x(const bool a, const bool b, const bool c, const bool d)
    {
        dacDevice.selectGain(
            a ? DacDevice::GAIN::X2 : DacDevice::GAIN::X1,
            b ? DacDevice::GAIN::X2 : DacDevice::GAIN::X1,
            c ? DacDevice::GAIN::X2 : DacDevice::GAIN::X1,
            d ? DacDevice::GAIN::X2 : DacDevice::GAIN::X1);
    }

    void update(const DACState &ss)
    {
        const uint16_t a = std::min(std::max(ss[0].value + ss[0].pitchbend, 0), 0xFFF);
        const uint16_t b = std::min(std::max(ss[1].value + ss[1].pitchbend, 0), 0xFFF);
        const uint16_t c = std::min(std::max(ss[2].value + ss[2].pitchbend, 0), 0xFFF);
        const uint16_t d = std::min(std::max(ss[3].value + ss[3].pitchbend, 0), 0xFFF);
        dacDevice.analogWrite(a, b, c, d);
        usleep(psc::io::I2C_DELAY_US);
        dacDevice.readRegisters();
        usleep(psc::io::I2C_DELAY_US);
    }

} // namespace psc::dac
