#include <esp_log.h>
// #include <esp_timer.h>

#include "psc-tft.h"

namespace psc::tft
{
    LGFX display;

    constexpr uint8_t ROWS[] = {9, 25, 38, 51, 64, 77, 90, 103, 116, 129};
    constexpr uint8_t COLS[] = {1, 25, 42, 59, 76, 93, 110, 127, 144};

    // void display_refresh(void *arg)
    // {
    //     __preDisplay()
    //
    //     display.setTextColor(TFT_WHITE);
    //     display.drawString("PSC Running", 0, 0);
    //
    //     __postDisplay();
    // }

    void setup()
    {
        display.init();
        display.light()->setBrightness(128);
        display.setTextSize(0);
        display.setColorDepth(16);
        display.fillScreen(TFT_BLACK);

        // const esp_timer_create_args_t display_refresh_Args = {
        //     .callback = &display_refresh,
        //     .arg = nullptr,
        //     .dispatch_method = ESP_TIMER_TASK,
        //     .name = "display_refresh",
        //     .skip_unhandled_events = false,
        // };
        // esp_timer_handle_t display_refresh;
        // ESP_ERROR_CHECK(esp_timer_create(&display_refresh_Args, &display_refresh));
        // ESP_ERROR_CHECK(esp_timer_start_periodic(display_refresh, 1e6));
    }

    void __preDisplay()
    {
        display.startWrite();
        display.fillScreen(TFT_BLACK);
        display.setTextDatum(BL_DATUM);
    }

    void __postDisplay()
    {
        display.endWrite();
    }

    void displayConfig(psc::config::Config &cfg)
    {
        __preDisplay();

        const auto dacColour = [=](const bool gain2x)
        {
            if (gain2x)
            {
                display.setTextColor(TFT_RED);
            }
            else
            {
                display.setTextColor(TFT_ORANGE);
            }
        };

        dacColour(cfg.dac[0].enable.gain2x);
        display.drawString("A", COLS[1], ROWS[0]);
        dacColour(cfg.dac[1].enable.gain2x);
        display.drawString("B", COLS[2], ROWS[0]);
        dacColour(cfg.dac[2].enable.gain2x);
        display.drawString("C", COLS[3], ROWS[0]);
        dacColour(cfg.dac[3].enable.gain2x);
        display.drawString("D", COLS[4], ROWS[0]);

        display.setTextColor(TFT_GREEN);
        display.drawString("1", COLS[5], ROWS[0]);
        display.drawString("2", COLS[6], ROWS[0]);
        display.drawString("3", COLS[7], ROWS[0]);
        display.setTextColor(TFT_CYAN);
        display.drawString("N", COLS[8], ROWS[0]);

        display.drawLine(0, 12, 160, 12, TFT_OLIVE);
        display.setTextColor(TFT_OLIVE);
        display.drawString("Ch.", COLS[0], ROWS[1]);
        display.drawString("En.", COLS[0], ROWS[2]);
        display.drawString("Mo.", COLS[0], ROWS[5]);
        display.drawString("Min", COLS[0], ROWS[6]);
        display.drawString("Max", COLS[0], ROWS[7]);
        display.drawString("CC", COLS[0], ROWS[8]);

        size_t c = 1;

        for (const auto &d : cfg.dac)
        {
            const auto col = COLS[c];
            display.setTextColor(TFT_WHITE);
            display.drawNumber(d.channel, col, ROWS[1]);
            switch (d.mode)
            {
            case psc::config::DACMode::NOTE:
                display.drawString("N", col, ROWS[5]);
                break;
            case psc::config::DACMode::VELOCITY:
                display.drawString("V", col, ROWS[5]);
                break;
            case psc::config::DACMode::CC7:
                display.drawString("7", col, ROWS[5]);
                display.drawNumber(d.CC[0], col, ROWS[8]);
                break;
            case psc::config::DACMode::CC14:
                display.drawString("14", col, ROWS[5]);
                display.drawNumber(d.CC[0], col, ROWS[8]);
                display.drawNumber(d.CC[1], col, ROWS[9]);
                break;
            }

            if (d.mode == psc::config::DACMode::NOTE || d.mode == psc::config::DACMode::VELOCITY || d.mode == psc::config::DACMode::CC7)
            {
                display.drawNumber(d.min, col, ROWS[6]);
                display.drawNumber(d.max, col, ROWS[7]);
            }

            if (d.enable.trigger)
            {
                display.setTextColor(TFT_WHITE);
            }
            else
            {
                display.setTextColor(TFT_DARKGREY);
            }
            display.drawString("T", col, ROWS[2]);

            // Gate does not apply to CC modes
            if (d.mode == psc::config::DACMode::NOTE || d.mode == psc::config::DACMode::VELOCITY)
            {
                if (d.enable.gate)
                {
                    display.setTextColor(TFT_WHITE);
                }
                else
                {
                    display.setTextColor(TFT_DARKGREY);
                }
                display.drawString("G", col, ROWS[3]);
            }

            if (d.enable.value)
            {
                display.setTextColor(TFT_WHITE);
            }
            else
            {
                display.setTextColor(TFT_DARKGREY);
            }
            display.drawString("V", col, ROWS[4]);

            c++;
        }

        for (const auto &p : cfg.psgv)
        {
            const auto col = COLS[c];
            display.setTextColor(TFT_WHITE);
            display.drawNumber(p.channel, col, ROWS[1]);
            if (!p.enable)
            {
                display.setTextColor(TFT_DARKGREY);
            }
            display.drawString("V", col, ROWS[4]);

            c++;
        }

        for (const auto &p : cfg.psgn)
        {
            const auto col = COLS[c];
            display.setTextColor(TFT_WHITE);
            display.drawNumber(p.channel, col, ROWS[1]);

            switch (p.mode)
            {
            case psc::config::PSGNMode::NOTE:
                display.drawString("N", col, ROWS[5]);
                break;
            case psc::config::PSGNMode::VELOCITY:
                display.drawString("V", col, ROWS[5]);
                break;
            case psc::config::PSGNMode::CC7:
                display.drawString("7", col, ROWS[5]);
                display.drawNumber(p.CC[0], col, ROWS[8]);
                break;
            }

            if (p.mixmask & 0b0001 && p.mixmask & 0b0010)
            {
                display.setTextColor(TFT_WHITE);
            }
            else
            {
                display.setTextColor(TFT_DARKGREY);
            }
            display.drawString("A", col, ROWS[2]);

            if (p.mixmask & 0b0001 && p.mixmask & 0b0100)
            {
                display.setTextColor(TFT_WHITE);
            }
            else
            {
                display.setTextColor(TFT_DARKGREY);
            }
            display.drawString("B", col, ROWS[3]);

            if (p.mixmask & 0b0001 && p.mixmask & 0b1000)
            {
                display.setTextColor(TFT_WHITE);
            }
            else
            {
                display.setTextColor(TFT_DARKGREY);
            }
            display.drawString("C", col, ROWS[4]);

            c++;
        }

        __postDisplay();
    }

} // namespace psc::tft
