// OS
// #include <esp_log.h>
#include <driver/gpio.h>
#include <driver/ledc.h>

// App
#include "psc-ay.h"

namespace psc::ay
{
    // static const char *TAG = "AY";

    static dedic_gpio_bundle_handle_t portD;

    enum class Control
    {
        INACTIVE = 0b00,
        READ = 0b01,
        WRITE = 0b10,
        ADDRESS = 0b11
    };

    void setup_clock()
    {
        // esp-idf LEDC PWM clock generator
        // Prepare and then apply the LEDC PWM timer configuration
        ledc_timer_config_t ledc_timer = {
            .speed_mode = LEDC_LOW_SPEED_MODE, // ??
            .duty_resolution = LEDC_TIMER_2_BIT,
            .timer_num = LEDC_TIMER_0,
            .freq_hz = 2000000, // Set output frequency at 2MHz
            .clk_cfg = LEDC_AUTO_CLK,
        };
        ESP_ERROR_CHECK(ledc_timer_config(&ledc_timer));

        // Prepare and then apply the LEDC PWM channel configuration
        ledc_channel_config_t ledc_channel = {
            .gpio_num = AY_CLK,
            .speed_mode = LEDC_LOW_SPEED_MODE, // ??
            .channel = LEDC_CHANNEL_0,
            .intr_type = LEDC_INTR_DISABLE,
            .timer_sel = LEDC_TIMER_0,
            .duty = 1, // 50% of 2**.duty_resolution
            .hpoint = 0,
            .flags = {
                .output_invert = 0,
            },
        };
        ESP_ERROR_CHECK(ledc_channel_config(&ledc_channel));
    }

    void setup_gpio()
    {
        // configure GPIO "PORT D" - data; output only
        const int port_D_gpios[] = {AY_PORT_D_PINS}; //  DA0..DA7

        gpio_config_t io_D_conf = {
            .pin_bit_mask = 0,
            .mode = GPIO_MODE_OUTPUT,
            .pull_up_en = GPIO_PULLUP_DISABLE,
            .pull_down_en = GPIO_PULLDOWN_ENABLE,
            .intr_type = GPIO_INTR_DISABLE,
        };
        for (int i = 0; i < sizeof(port_D_gpios) / sizeof(port_D_gpios[0]); i++)
        {
            io_D_conf.pin_bit_mask |= 1ULL << port_D_gpios[i];
        }
        gpio_config(&io_D_conf);
        dedic_gpio_bundle_config_t bundle_D_config = {
            .gpio_array = port_D_gpios,
            .array_size = sizeof(port_D_gpios) / sizeof(port_D_gpios[0]),
            .flags = {
                .in_en = 0,
                .in_invert = 0,
                .out_en = 1,
                .out_invert = 0,
            },
        };
        ESP_ERROR_CHECK(dedic_gpio_new_bundle(&bundle_D_config, &portD));

        // configure GPIO "PORT C" - control; output only
        const int port_C_gpios[] = {AY_BC1, AY_BDIR};

        gpio_config_t io_C_conf = {
            .pin_bit_mask = 0,
            .mode = GPIO_MODE_OUTPUT,
            .pull_up_en = GPIO_PULLUP_DISABLE,
            .pull_down_en = GPIO_PULLDOWN_ENABLE,
            .intr_type = GPIO_INTR_DISABLE,
        };
        for (int i = 0; i < sizeof(port_C_gpios) / sizeof(port_C_gpios[0]); i++)
        {
            io_C_conf.pin_bit_mask |= 1ULL << port_C_gpios[i];
        }
        gpio_config(&io_C_conf);

        // Manage the control pins manually
        // not enough SoC resources to bundle these
        gpio_set_level((gpio_num_t)AY_BC1, 0);
        gpio_set_level((gpio_num_t)AY_BDIR, 0);
    }

    void set_control(Control mode)
    {
        // BC1 must be changed before BDIR

        const auto bc1 = (uint8_t)mode & 0b01 ? 1 : 0;
        /*const auto rbc1 =*/gpio_set_level((gpio_num_t)AY_BC1, bc1);
        // ESP_LOGI(TAG, "set AY_BC1  %d 0x%x %d : 0x%x", AY_BC1, (uint8_t)mode, bc1, rbc1);

        const auto bdir = (uint8_t)mode & 0b10 ? 1 : 0;
        /*const auto rbdir =*/gpio_set_level((gpio_num_t)AY_BDIR, bdir);
        // ESP_LOGI(TAG, "set AY_BDIR %d 0x%x %d : 0x%x", AY_BDIR, (uint8_t)mode, bdir, rbdir);
    }

    void set_data(unsigned char data)
    {
        dedic_gpio_bundle_write(portD, 0xFF, data);
    }

    void writeRegister(uint8_t reg, uint8_t value)
    {
        set_data(reg & 0x0F);
        set_control(Control::ADDRESS);
        usleep(3);
        set_control(Control::INACTIVE);

        usleep(1);

        set_data(value);
        usleep(1);
        set_control(Control::WRITE);
        usleep(5);
        set_control(Control::INACTIVE);
    }

    void writeRegisters(const AY38910::DataFrame frm)
    {
        for (int i = 0; i < frm.size(); ++i)
        {
            writeRegister(i, frm[i]);
        }
    }

    void setup()
    {
        setup_clock();
        setup_gpio();

        // Be sure to kill all possible sound by setting volume to zero
        writeRegister(AY38910::Registers::REG_LVL_A, 0x00);
        writeRegister(AY38910::Registers::REG_LVL_B, 0x00);
        writeRegister(AY38910::Registers::REG_LVL_C, 0x00);
    }

} // namespace psc::ay
