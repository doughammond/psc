#pragma once

namespace psc
{
    // Pin assignments for esp32s3 devkitc 1

    // NB PSRAM on 35, 36, 37
    // NB RGBLED is on GPIO 38

    // USB D+ on 20, D- on 19

    // AY-3-8910
#define AY_BC1 12
#define AY_BDIR 13
#define AY_CLK 14
#define AY_PORT_D_PINS 15, 16, 17, 18, 8, 9, 10, 11

    // I2C bus
#define DAC_SCL 41
#define DAC_SDA 40

    // MCP4728 DAC device
#define DAC_NLDAC 39

    // MCP23018 IO device
#define MCP23017_ADDR 0x20

    // SPI Bus
#define TFT_MISO -1 // NC
#define TFT_MOSI 4  // Bu
#define TFT_SCLK 5  // Yw
#define TFT_DC 47   // Gn - pin 28, schematic says GPIO 33, but is GPIO 47 on the devkitc

    // TFT display
#define TFT_CS 6  // Or
#define TFT_RST 1 // Wh; Connect reset to ensure display initialises
#define TFT_BUSY -1
#define TFT_BL 21 // Pu; PWM backlight control

} // namespace psc