#pragma once

#include <LovyanGFX.hpp>

#include "psc-board.h"
#include "psc-config.h"

namespace psc::tft
{
    // static const char *TAG = "tft";

    class LGFX : public lgfx::LGFX_Device
    {
        lgfx::Bus_SPI _bus_instance;
        lgfx::Panel_ST7735S _panel_instance;
        lgfx::Light_PWM _light_instance;

    public:
        LGFX(void)
        {
            {
                auto cfg = _bus_instance.config();

                cfg.spi_host = SPI2_HOST;

                cfg.spi_mode = 0;
                cfg.freq_write = 20000000;
                cfg.freq_read = 2500000;
                cfg.spi_3wire = false;
                cfg.use_lock = true;
                cfg.dma_channel = SPI_DMA_CH_AUTO;

                cfg.pin_sclk = TFT_SCLK;
                cfg.pin_mosi = TFT_MOSI;
                cfg.pin_miso = TFT_MISO;
                cfg.pin_dc = TFT_DC;

                _bus_instance.config(cfg);
                _panel_instance.setBus(&_bus_instance);
            }

            {
                auto cfg = _panel_instance.config();

                cfg.pin_cs = TFT_CS;
                cfg.pin_rst = TFT_RST;
                cfg.pin_busy = TFT_BUSY;

                cfg.panel_width = 128;
                cfg.panel_height = 160;
                cfg.offset_x = 2;
                cfg.offset_y = 1;
                cfg.offset_rotation = 1;
                cfg.dummy_read_pixel = 8;
                cfg.dummy_read_bits = 1;
                cfg.readable = false;
                cfg.invert = false;
                cfg.rgb_order = true;
                cfg.dlen_16bit = false;
                cfg.bus_shared = true;

                _panel_instance.config(cfg);
            }

            {
                auto cfg = _light_instance.config();

                cfg.pin_bl = TFT_BL;
                cfg.invert = false;
                cfg.freq = 44100;
                cfg.pwm_channel = 7;

                _light_instance.config(cfg);
                _panel_instance.setLight(&_light_instance);
            }

            setPanel(&_panel_instance);
        }
    };

    void setup();
    void displayConfig(psc::config::Config &cfg);

} // namespace psc::tft
