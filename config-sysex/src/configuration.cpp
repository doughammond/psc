#include <cmath>

#include "configuration.h"

constexpr char _PSC_DEFAULT_CONFIG[] =
"dac.channel.0=1\n"
"dac.channel.1=2\n"
"dac.channel.2=3\n"
"dac.channel.3=4\n"
"dac.enabled.0=1\n"
"dac.enabled.1=1\n"
"dac.enabled.2=1\n"
"dac.enabled.3=1\n"
"dac.pitchbend.0=1\n"
"dac.pitchbend.1=1\n"
"dac.pitchbend.2=1\n"
"dac.pitchbend.3=1\n"
"dac.gate.0=1\n"
"dac.gate.1=1\n"
"dac.gate.2=1\n"
"dac.gate.3=1\n"
"dac.trig.0=1\n"
"dac.trig.1=1\n"
"dac.trig.2=1\n"
"dac.trig.3=1\n"
"dac.gain2x.0=0\n"
"dac.gain2x.1=0\n"
"dac.gain2x.2=0\n"
"dac.gain2x.3=0\n"
"dac.mode.0=0\n"
"dac.mode.1=0\n"
"dac.mode.2=0\n"
"dac.mode.3=0\n"
"dac.range_min.0=60\n"
"dac.range_min.1=60\n"
"dac.range_min.2=60\n"
"dac.range_min.3=60\n"
"dac.range_max.0=77\n"
"dac.range_max.1=77\n"
"dac.range_max.2=77\n"
"dac.range_max.3=77\n"
"dac.cc_msb.0=0\n"
"dac.cc_msb.1=0\n"
"dac.cc_msb.2=0\n"
"dac.cc_msb.3=0\n"
"dac.cc_lsb.0=0\n"
"dac.cc_lsb.1=0\n"
"dac.cc_lsb.2=0\n"
"dac.cc_lsb.3=0\n"
"psg.channel.0=5\n"
"psg.channel.1=6\n"
"psg.channel.2=7\n"
"psg.channel.3=8\n"
"psg.enabled.0=1\n"
"psg.enabled.1=1\n"
"psg.enabled.2=1\n"
"psg.enabled.3=1\n"
"psg.mode.0=0\n"
"psg.mode.1=0\n"
"psg.mode.2=0\n"
"psg.mode.3=0\n"
"psg.cc_msb.0=0\n"
"psg.cc_msb.1=0\n"
"psg.cc_msb.2=0\n"
"psg.cc_msb.3=0\n"
"psg.mixtoA.0=0\n"
"psg.mixtoA.1=0\n"
"psg.mixtoA.2=0\n"
"psg.mixtoA.3=0\n"
"psg.mixtoB.0=0\n"
"psg.mixtoB.1=0\n"
"psg.mixtoB.2=0\n"
"psg.mixtoB.3=0\n"
"psg.mixtoC.0=0\n"
"psg.mixtoC.1=0\n"
"psg.mixtoC.2=0\n"
"psg.mixtoC.3=0\n"
;

namespace DataModel {

PSC::PSC(QObject *parent) : QObject(parent)
{

}

DAC::DAC(QObject *parent) : QObject(parent)
{

}

QString DAC::serialiseToConfigString(const uint8_t &index) const
{
    QString conf;
    conf += QStringLiteral("dac.channel.%1=%2\n").arg(index).arg(channel);
    conf += QStringLiteral("dac.enabled.%1=%2\n").arg(index).arg(enableChannel);
    conf += QStringLiteral("dac.pitchbend.%1=%2\n").arg(index).arg(enablePitchbend);
    conf += QStringLiteral("dac.gate.%1=%2\n").arg(index).arg(enableGate);
    conf += QStringLiteral("dac.trig.%1=%2\n").arg(index).arg(enableTrig);
    conf += QStringLiteral("dac.gain2x.%1=%2\n").arg(index).arg(enableGain2x);
    conf += QStringLiteral("dac.mode.%1=%2\n").arg(index).arg(mode);
    conf += QStringLiteral("dac.range_min.%1=%2\n").arg(index).arg(range_min);
    conf += QStringLiteral("dac.range_max.%1=%2\n").arg(index).arg(range_max);
    conf += QStringLiteral("dac.cc_msb.%1=%2\n").arg(index).arg(cc_msb);
    conf += QStringLiteral("dac.cc_lsb.%1=%2\n").arg(index).arg(cc_lsb);
    return conf;
}

void DAC::deserialiseConfigString(const QString &k, const QString &v)
{
    bool castOk = false;
    const auto iv = v.toUInt(&castOk, 10);
    if (!castOk) {
        qDebug() << "could not deserialise value for DAC key" << k;
        return;
    }
    qDebug() << name << "deserialise" << k << ":" << iv;
    bool didChange = false;
    if (k.contains("channel"))
    {
        const auto next = static_cast<uint8_t>(iv);
        if (channel != next) {
            channel = next;
            didChange = true;
        }
    }
    if (k.contains("enabled")) {
        const auto next = static_cast<bool>(iv);
        if (enableChannel != next) {
            enableChannel = next;
            didChange = true;
        }
    }
    if (k.contains("pitchbend")) {
        const auto next = static_cast<bool>(iv);
        if (enablePitchbend != next) {
            enablePitchbend = next;
            didChange = true;
        }
    }
    if (k.contains("gate")) {
        const auto next = static_cast<bool>(iv);
        if (enableGate != next) {
            enableGate = next;
            didChange = true;
        }
    }
    if (k.contains("trig")) {
        const auto next = static_cast<bool>(iv);
        if (enableTrig != next) {
            enableTrig = next;
            didChange = true;
        }
    }
    if (k.contains("gain2x")) {
        const auto next = static_cast<bool>(iv);
        if (enableGain2x != next) {
            enableGain2x = next;
            didChange = true;
        }
    }
    if (k.contains("range_min"))
    {
        const auto next = static_cast<uint8_t>(iv);
        if (range_min != next) {
            range_min = next;
            didChange = true;
        }
    }
    if (k.contains("range_max"))
    {
        const auto next = static_cast<uint8_t>(iv);
        if (range_max != next) {
            range_max = next;
            didChange = true;
        }
    }
    if (k.contains("cc_msb"))
    {
        const auto next = static_cast<uint8_t>(iv);
        if (cc_msb != next) {
            cc_msb = next;
            didChange = true;
        }
    }
    if (k.contains("c_lsb"))
    {
        const auto next = static_cast<uint8_t>(iv);
        if (cc_lsb != next) {
            cc_lsb = next;
            didChange = true;
        }
    }
    if (k.contains("mode"))
    {
        const auto next = static_cast<MidiMode>(iv);
        if (mode != next) {
            mode = next;
            didChange = true;
        }
    }

    if (didChange) {
        emit changed();
    }
}

void DAC::setChannel(const uint8_t &newchannel)
{
    if (channel != newchannel) {
        channel = newchannel;
        emit changed();
    }
}

void DAC::setEnableChannel(const bool &enabled)
{
    if (enableChannel != enabled) {
        enableChannel = enabled;
        emit changed();
    }
}

void DAC::setEnablePitchbend(const bool &enabled)
{
    if (enablePitchbend != enabled) {
        enablePitchbend = enabled;
        emit changed();
    }
}

void DAC::setEnableGate(const bool &enabled)
{
    if (enableGate != enabled) {
        enableGate = enabled;
        emit changed();
    }
}

void DAC::setEnableTrig(const bool &enabled)
{
    if (enableTrig != enabled) {
        enableTrig = enabled;
        emit changed();
    }
}

void DAC::setEnableGain2x(const bool &enabled)
{
    if (enableGain2x != enabled) {
        enableGain2x = enabled;
        emit changed();
    }
}

void DAC::setMode(const MidiMode &newmode)
{
    if (mode != newmode) {
        mode = newmode;
        emit changed();
    }
}

void DAC::setRangeMin(const uint8_t &newmin)
{
    if (range_min != newmin) {
        range_min = newmin;
        emit changed();
    }
}

void DAC::setRangeMax(const uint8_t &newmax)
{
    if (range_max != newmax) {
        range_max = newmax;
        emit changed();
    }
}

void DAC::setCCMSB(const uint8_t &newmsb)
{
    if (cc_msb != newmsb) {
        cc_msb = newmsb;
        emit changed();
    }
}

void DAC::setCCLSB(const uint8_t &newlsb)
{
    if (cc_lsb != newlsb) {
        cc_lsb = newlsb;
        emit changed();
    }
}

PSG::PSG(QObject *parent) : QObject(parent)
{

}

QString PSG::serialiseToConfigString(const uint8_t &index) const
{
    QString conf;
    conf += QStringLiteral("psg.channel.%1=%2\n").arg(index).arg(channel);
    conf += QStringLiteral("psg.enabled.%1=%2\n").arg(index).arg(enableChannel);
    conf += QStringLiteral("psg.mode.%1=%2\n").arg(index).arg(mode);
    conf += QStringLiteral("psg.cc_msb.%1=%2\n").arg(index).arg(cc_msb);
    conf += QStringLiteral("psg.mixtoA.%1=%2\n").arg(index).arg(mixtoA);
    conf += QStringLiteral("psg.mixtoB.%1=%2\n").arg(index).arg(mixtoB);
    conf += QStringLiteral("psg.mixtoC.%1=%2\n").arg(index).arg(mixtoC);
    return conf;
}

void PSG::deserialiseConfigString(const QString &k, const QString &v)
{
    bool castOk = false;
    const auto iv = v.toUInt(&castOk, 10);
    if (!castOk) {
        qDebug() << "could not deserialise value for PSG key" << k;
        return;
    }
    qDebug() << name << "deserialise" << k << ":" << iv;
    bool didChange = false;
    if (k.contains("channel"))
    {
        const auto next = static_cast<uint8_t>(iv);
        if (channel != next) {
            channel = next;
            didChange = true;
        }
    }
    if (k.contains("enabled")) {
        const auto next = static_cast<bool>(iv);
        if (enableChannel != next) {
            enableChannel = next;
            didChange = true;
        }
    }
    if (k.contains("mode"))
    {
        const auto next = static_cast<MidiMode>(iv);
        if (mode != next) {
            mode = next;
            didChange = true;
        }
    }
    if (k.contains("cc_msb"))
    {
        const auto next = static_cast<uint8_t>(iv);
        if (cc_msb != next) {
            cc_msb = next;
            didChange = true;
        }
    }
    if (k.contains("mixtoA"))
    {
        const auto next = static_cast<bool>(iv);
        if (mixtoA != next) {
            mixtoA = next;
            didChange = true;
        }
    }
    if (k.contains("mixtoB"))
    {
        const auto next = static_cast<bool>(iv);
        if (mixtoB != next) {
            mixtoB = next;
            didChange = true;
        }
    }
    if (k.contains("mixtoC"))
    {
        const auto next = static_cast<bool>(iv);
        if (mixtoC != next) {
            mixtoC = next;
            didChange = true;
        }
    }

    if (didChange) {
        emit changed();
    }
}

void PSG::setChannel(const uint8_t &newchannel)
{
    if (channel != newchannel) {
        channel = newchannel;
        emit changed();
    }
}

void PSG::setEnableChannel(const bool &enabled)
{
    if (enableChannel != enabled) {
        enableChannel = enabled;
        emit changed();
    }
}

void PSG::setMode(const MidiMode &newmode)
{
    if (mode != newmode) {
        mode = newmode;
        emit changed();
    }
}

void PSG::setCCMSB(const uint8_t &newmsb)
{
    if (cc_msb != newmsb) {
        cc_msb = newmsb;
        emit changed();
    }
}

void PSG::setMixtoA(const bool &enabled)
{
    if (mixtoA != enabled) {
        mixtoA = enabled;
        emit changed();
    }
}

void PSG::setMixtoB(const bool &enabled)
{
    if (mixtoB != enabled) {
        mixtoB = enabled;
        emit changed();
    }
}

void PSG::setMixtoC(const bool &enabled)
{
    if (mixtoC != enabled) {
        mixtoC = enabled;
        emit changed();
    }
}

} // namespace DataModel

Configuration::Configuration(QObject *parent) : QObject(parent)
{
    m_dacs[0].name = "DACA";
    m_dacs[1].name = "DACB";
    m_dacs[2].name = "DACC";
    m_dacs[3].name = "DACD";

    for (auto &dac : m_dacs) {
        connect(&dac, &DataModel::DAC::changed, this, &Configuration::serialiseAndSave);
        connect(&dac, &DataModel::DAC::changed, this, &Configuration::serialiseToSysEx);
    }

    m_psgs[0].name = "PSGA";
    m_psgs[1].name = "PSGB";
    m_psgs[2].name = "PSGC";
    m_psgs[3].name = "PSGN";

    for (auto &psg : m_psgs) {
        connect(&psg, &DataModel::PSG::changed, this, &Configuration::serialiseAndSave);
        connect(&psg, &DataModel::PSG::changed, this, &Configuration::serialiseToSysEx);
    }
}

void Configuration::loadFromSettings()
{
    QSettings settings;
    const auto conf = settings.value("config");
    if (conf.isValid()) {
        qDebug() << "Loading saved config" << conf;
        deserialise(conf.toString());
    }
    else {
        deserialise(_PSC_DEFAULT_CONFIG);
        qDebug() << "Saved config not valid; will load defaults" << conf;
    }
}

void Configuration::serialiseAndSave()
{
    QString conf;
    conf += m_dacs[0].serialiseToConfigString(0);
    conf += m_dacs[1].serialiseToConfigString(1);
    conf += m_dacs[2].serialiseToConfigString(2);
    conf += m_dacs[3].serialiseToConfigString(3);

    conf += m_psgs[0].serialiseToConfigString(0);
    conf += m_psgs[1].serialiseToConfigString(1);
    conf += m_psgs[2].serialiseToConfigString(2);
    conf += m_psgs[3].serialiseToConfigString(3);

    qDebug() << "Saving config" << conf;
    QSettings settings;
    settings.setValue("config", conf);
}

void Configuration::serialiseToSysEx() {
    // TODO: config serialisation to Sysex is (mostly) duplicated in esp32/src/config.cpp

    QByteArray data;

    // To reduce the message size, we should iterate the config types and
    // values and group (mask) together the outputs which have the same value

    uint8_t cfg = 0x00; // Channel
    std::map<uint8_t, uint16_t> channel_map;
    for (int i = 0; i< 4; ++i) {
        const auto dacchannel = m_dacs[i].channel - 1;
        channel_map[dacchannel] |= 1 << i;

        const auto psgchannel = m_psgs[i].channel - 1;
        channel_map[psgchannel] |= 1 << (i + 8);
    }
    for (const auto &p : channel_map) {
        data += cfg;
        data += p.second & 0xFF;
        data += p.second >> 8;
        data += p.first;
    }

    cfg = 0x01; // Enable
    std::map<uint8_t, uint16_t> enable_map;
    for (int i = 0; i< 4; ++i) {
        const auto dacenable = (
            (m_dacs[i].enableChannel   ? 0b00001 : 0b00000) |
            (m_dacs[i].enableGate      ? 0b00010 : 0b00000) |
            (m_dacs[i].enableTrig      ? 0b00100 : 0b00000) |
            (m_dacs[i].enableGain2x    ? 0b01000 : 0b00000) |
            (m_dacs[i].enablePitchbend ? 0b10000 : 0b00000)
        );
        enable_map[dacenable] |= 1 << i;

        const auto psgenable = (
            (m_psgs[i].enableChannel ? 0b0001 : 0b0000) |
            (m_psgs[i].mixtoA        ? 0b0010 : 0b0000) |
            (m_psgs[i].mixtoB        ? 0b0100 : 0b0000) |
            (m_psgs[i].mixtoC        ? 0b1000 : 0b0000)
        );
        enable_map[psgenable] |= 1 << (i + 8);
    }
    for (const auto &p : enable_map) {
        data += cfg;
        data += p.second & 0xFF;
        data += p.second >> 8;
        data += p.first;
    }

    cfg = 0x02; // Mode
    std::map<uint8_t, uint16_t> mode_map;
    for (int i = 0; i< 4; ++i) {
        const auto dacmode = m_dacs[i].mode;
        mode_map[dacmode] |= 1 << i;

        const auto psgmode = m_psgs[i].mode;
        mode_map[psgmode] |= 1 << (i + 8);
    }
    for (const auto &p : mode_map) {
        data += cfg;
        data += p.second & 0xFF;
        data += p.second >> 8;
        data += p.first;
    }

    cfg = 0x03; // Min - DAC only
    std::map<uint8_t, uint8_t> min_map;
    for (int i = 0; i< 4; ++i) {
        const auto dacmin = m_dacs[i].range_min & 0x7F;
        min_map[dacmin] |= 1 << i;
    }
    for (const auto &p : min_map) {
        data += cfg;
        data += p.second;
        data += '\x00';
        data += p.first;
    }

    cfg = 0x04; // Max - DAC only
    std::map<uint8_t, uint8_t> max_map;
    for (int i = 0; i< 4; ++i) {
        const auto dacmax = m_dacs[i].range_max & 0x7F;
        max_map[dacmax] |= 1 << i;
    }
    for (const auto &p : max_map) {
        data += cfg;
        data += p.second;
        data += '\x00';
        data += p.first;
    }

    cfg = 0x05; // CC7
    std::map<uint8_t, uint16_t> cc7_map;
    for (int i = 0; i< 4; ++i) {
        const auto daccc7 = m_dacs[i].cc_msb & 0x7F;
        cc7_map[daccc7] |= 1 << i;

        const auto psgcc7 = m_psgs[i].cc_msb & 0x7F;
        cc7_map[psgcc7] |= 1 << (i + 8);
    }
    for (const auto &p : cc7_map) {
        data += cfg;
        data += p.second & 0xFF;
        data += p.second >> 8;
        data += p.first;
    }

    cfg = 0x06; // CC14 - DAC only
    std::map<uint8_t, uint8_t> cc14_map;
    for (int i = 0; i< 4; ++i) {
        const auto daccc14 = m_dacs[i].cc_lsb & 0x7F;
        cc14_map[daccc14] |= 1 << i;
    }
    for (const auto &p : cc14_map) {
        data += cfg;
        data += p.second & 0xFF;
        data += '\x00';
        data += p.first;
    }

    QByteArray sysex;
    sysex += '\xF0'; // start SysEx
    sysex += QByteArray("\x00\x60\x00\x00\x00", 5); // config header
    sysex += data;
    sysex  += '\xF7'; // end SysEx

    qDebug() << "SysEx data " << sysex.length() << sysex;

    emit sysExData(sysex);
}

void Configuration::deserialise(const QString &config)
{
    for (const auto &line : config.split("\n"))
    {
        if (line.length() > 0)
        {
            const auto kv = line.split("=");
            if (kv.length() == 2)
            {
                const auto k = kv[0];
                const auto v = kv[1];
                const auto kp = k.split(".");
                if (kp.size() == 3 && kp[0] == "dac")
                {
                    bool castOk = false;
                    const auto dac = static_cast<uint8_t>(kp[2].toUInt(&castOk, 10));
                    if (!castOk || dac > 3) {
                        qDebug() << "DAC deserialisation parameters not ok {" << castOk << "," << dac << "}";
                        continue;
                    }
                    m_dacs[dac].deserialiseConfigString(k, v);
                }
                if (kp.size() == 3 && kp[0] == "psg")
                {
                    bool castOk = false;
                    const auto psg = static_cast<uint8_t>(kp[2].toUInt(&castOk, 10));
                    if (!castOk || psg > 3) {
                        qDebug() << "PSG deserialisation parameters not ok {" << castOk << "," << psg << "}";
                        continue;
                    }
                    m_psgs[psg].deserialiseConfigString(k, v);
                }
            }
        }
    }
}
