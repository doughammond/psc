#pragma once

#include <QCoreApplication>
#include <QThread>

#include "midi.h"
#include "ui/mainwindow.h"

class PSCApp : public QObject
{
Q_OBJECT

public:
    PSCApp();
    virtual ~PSCApp();
    void show();

private:
    MIDIManager *m_midi;

    MainWindow m_ui;
};
