#include "midi.h"

MIDIManager::MIDIManager(QObject *parent)
    : QObject(parent)
{
}

MIDIManager::~MIDIManager()
{
    closeMidiDevice();
}

QStringList MIDIManager::listMidiDevices()
{
    auto midiOuts = juce::MidiOutput::getDevices();
    QStringList midiDeviceNames;
    for (auto &m : midiOuts) {
        midiDeviceNames << m.toStdString().c_str();
        qDebug() << "MIDI Output" << m.toStdString().c_str();
    }
    return midiDeviceNames;
}

void MIDIManager::openMidiDevice(const int index)
{
    closeMidiDevice();

    qDebug() << "try to open MIDI device index" << index;
    auto output = juce::MidiOutput::openDevice(index);
    if (output != nullptr) {
        qDebug() << "MIDI Device opened" << output->getName().toStdString().c_str();
        m_midiOut = output;
        m_midiOut->startBackgroundThread();
    }
}

void MIDIManager::sendSysEx(const QByteArray &data)
{
    if (m_midiOut != nullptr) {
        const juce::MidiMessage mm(data.data(), data.length());
        m_midiOut->sendMessageNow(mm);
    }
}

void MIDIManager::closeMidiDevice()
{
    if (m_midiOut != nullptr) {
        m_midiOut->stopBackgroundThread();
        delete m_midiOut;
    }
}
