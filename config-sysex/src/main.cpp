#include <QGuiApplication>
#include <QApplication>

#include <JuceHeader.h>

#include "pscapp.h"

int main(int argc, char *argv[])
{
    ScopedJuceInitialiser_GUI juceInit;

    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName("doug@lon.dev");
    QCoreApplication::setOrganizationDomain("lon.dev");
    QCoreApplication::setApplicationName("PSC Control");

    QApplication app(argc, argv);
    PSCApp psc;
    psc.show();
    return app.exec();
}
