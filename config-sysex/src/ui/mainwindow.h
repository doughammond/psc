#pragma once

#include <QMainWindow>

#include "../configuration.h"

#include "ui_mainwindow.h"

namespace Ui
{
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void setMidiDeviceNames(const QStringList &deviceNames);

signals:
    void midiDeviceSelectionChanged(const int index);

private slots:
    void onDACChannelChange(const QString &num);
    void onDACChannelEnableChanged(const bool &state);
    void onDACPitchbendEnabledChanged(const bool &state);
    void onDACGateEnableChanged(const bool &state);
    void onDACTrigEnableChanged(const bool &state);
    void onDACGainChanged(const bool &state);
    void onDACModeChange(const bool &sel);
    void onDACRangeChange(const int &val);
    void onDACCCChange(const int &num);

    void onPSGChannelChange(const QString &num);
    void onPSGModeChange(const bool &sel);
    void onPSGChannelEnableChanged(const bool &state);
    void onPSGNMixEnableChanged(const bool &state);
    void onPSGCCChange(const int &num);

    void onDacChanged();
    void onPsgChanged();

public slots:
    void displaySysEx(const QByteArray &data);

private:
    void loadConfiguration();

    Ui::MainWindow *ui;
};
