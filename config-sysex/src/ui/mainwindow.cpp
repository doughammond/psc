#include <math.h>

#include <QDebug>

#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
                                          ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->cmbMidiDevices, SIGNAL(currentIndexChanged(int)), this, SIGNAL(midiDeviceSelectionChanged(int)));

    loadConfiguration();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::setMidiDeviceNames(const QStringList &deviceNames)
{
    ui->cmbMidiDevices->clear();
    for (auto name : deviceNames)
    {
        ui->cmbMidiDevices->addItem(name);
    }
}

void MainWindow::loadConfiguration()
{
    auto &config = Configuration::instance();

    connect(&config.getDac(0), &DataModel::DAC::changed, this, &MainWindow::onDacChanged);
    connect(&config.getDac(1), &DataModel::DAC::changed, this, &MainWindow::onDacChanged);
    connect(&config.getDac(2), &DataModel::DAC::changed, this, &MainWindow::onDacChanged);
    connect(&config.getDac(3), &DataModel::DAC::changed, this, &MainWindow::onDacChanged);

    connect(&config.getPsg(0), &DataModel::PSG::changed, this, &MainWindow::onPsgChanged);
    connect(&config.getPsg(1), &DataModel::PSG::changed, this, &MainWindow::onPsgChanged);
    connect(&config.getPsg(2), &DataModel::PSG::changed, this, &MainWindow::onPsgChanged);
    connect(&config.getPsg(3), &DataModel::PSG::changed, this, &MainWindow::onPsgChanged);

    config.loadFromSettings();
}

const QStringList dacIds = {"DACA", "DACB", "DACC", "DACD"};
const QStringList dacNames = {"DAC A", "DAC B", "DAC C", "DAC D"};

uint8_t dacNumberFromParamName(const QWidget *w)
{
    const auto paramName = w->property("objectName").toString();
    for (auto &name : dacIds)
    {
        if (paramName.contains(name))
        {
            return dacIds.indexOf(name);
        }
    }
    return 255;
}

void MainWindow::onDACChannelChange(const QString &num)
{
    const auto w = reinterpret_cast<QWidget *>(sender());
    qDebug() << "onDACChannelChange " << w << num;
    const auto dacNumber = dacNumberFromParamName(w);
    if (dacNumber < 4)
    {
        const auto channel = static_cast<uint8_t>(num.toUInt());
        auto &dac = Configuration::instance().getDac(dacNumber);
        if (dac.channel != channel)
        {
            dac.setChannel(channel);
            Configuration::instance().serialiseAndSave();
        }
    }
}

void MainWindow::onDACChannelEnableChanged(const bool &state)
{
    const auto w = reinterpret_cast<QWidget *>(sender());
    qDebug() << "onDACChannelEnableChanged " << w << state;
    const auto dacNumber = dacNumberFromParamName(w);
    if (dacNumber < 4)
    {
        auto &dac = Configuration::instance().getDac(dacNumber);
        if (dac.enableChannel != state)
        {
            dac.setEnableChannel(state);
        }
    }
}

void MainWindow::onDACPitchbendEnabledChanged(const bool &state)
{
    const auto w = reinterpret_cast<QWidget *>(sender());
    qDebug() << "onDACChannelPitchBendChanged " << w << state;
    const auto dacNumber = dacNumberFromParamName(w);
    if (dacNumber < 4)
    {
        auto &dac = Configuration::instance().getDac(dacNumber);
        if (dac.enablePitchbend != state)
        {
            dac.setEnablePitchbend(state);
        }
    }
}

void MainWindow::onDACGateEnableChanged(const bool &state)
{
    const auto w = reinterpret_cast<QWidget *>(sender());
    qDebug() << "onDACGateEnableChanged " << w << state;
    const auto dacNumber = dacNumberFromParamName(w);
    if (dacNumber < 4)
    {
        auto &dac = Configuration::instance().getDac(dacNumber);
        if (dac.enableGate != state)
        {
            dac.setEnableGate(state);
        }
    }
}

void MainWindow::onDACTrigEnableChanged(const bool &state)
{
    const auto w = reinterpret_cast<QWidget *>(sender());
    qDebug() << "onDACTrigEnableChanged " << w << state;
    const auto dacNumber = dacNumberFromParamName(w);
    if (dacNumber < 4)
    {
        auto &dac = Configuration::instance().getDac(dacNumber);
        if (dac.enableTrig != state)
        {
            dac.setEnableTrig(state);
        }
    }
}

void MainWindow::onDACGainChanged(const bool &state)
{
    const auto w = reinterpret_cast<QRadioButton *>(sender());
    qDebug() << "onDACGainChanged " << w << state;
    const auto dacNumber = dacNumberFromParamName(w);
    if (dacNumber < 4)
    {
        const auto paramName = w->property("objectName").toString();
        auto &dac = Configuration::instance().getDac(dacNumber);
        auto enableGain2x = dac.enableGain2x;
        if (paramName.contains("1") && w->isChecked())
        {
            enableGain2x = false;
        }
        if (paramName.contains("2") && w->isChecked())
        {
            enableGain2x = true;
        }
        if (dac.enableGain2x != enableGain2x)
        {
            dac.setEnableGain2x(enableGain2x);
        }
    }
}

void MainWindow::onDACModeChange(const bool &sel)
{
    const auto w = reinterpret_cast<QRadioButton *>(sender());
    qDebug() << "onDACModeChange " << w << sel;
    const auto dacNumber = dacNumberFromParamName(w);
    if (dacNumber < 4)
    {
        const auto paramName = w->property("objectName").toString();
        auto &dac = Configuration::instance().getDac(dacNumber);
        auto mode = dac.mode;
        if (paramName.contains("Note") && w->isChecked())
        {
            mode = DataModel::MidiMode::NOTE_VALUE;
        }
        if (paramName.contains("Velocity") && w->isChecked())
        {
            mode = DataModel::MidiMode::NOTE_VELOCITY;
        }
        if (paramName.contains("CC7") && w->isChecked())
        {
            mode = DataModel::MidiMode::CC_7;
        }
        if (paramName.contains("CC14") && w->isChecked())
        {
            mode = DataModel::MidiMode::CC_14;
        }
        if (dac.mode != mode)
        {
            dac.setMode(mode);
        }
    }
}

constexpr char MIDI_NOTE_NAMES[12][3] = {
    "C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "Bb", "B"};

void MainWindow::onDACRangeChange(const int &val)
{
    const auto w = reinterpret_cast<QSpinBox *>(sender());
    qDebug() << "onDACRangeChange " << w << val;

    // Update the data model
    const auto dacNumber = dacNumberFromParamName(w);
    if (dacNumber < 4)
    {
        auto &dac = Configuration::instance().getDac(dacNumber);

        if (dac.mode == DataModel::MidiMode::NOTE_VALUE)
        {
            // Set the note name as the input "suffix"
            const auto noteName = QString(MIDI_NOTE_NAMES[val % 12]);
            const auto noteOctave = std::floor(static_cast<double>(val) / 12.0) - 1;
            w->setSuffix(" " + noteName + QString("%1").arg(noteOctave));
        }
        else
        {
            w->setSuffix("");
        }

        const auto paramName = w->property("objectName").toString();
        uint8_t range_min = dac.range_min;
        uint8_t range_max = dac.range_max;
        if (paramName.contains("Min"))
        {
            range_min = val;
        }
        if (paramName.contains("Max"))
        {
            range_max = val;
        }
        if (dac.range_min != range_min)
        {
            dac.setRangeMin(range_min);
            Configuration::instance().serialiseAndSave();
        }
        if (dac.range_max != range_max)
        {
            dac.setRangeMax(range_max);
            Configuration::instance().serialiseAndSave();
        }
    }
}

void MainWindow::onDACCCChange(const int &num)
{
    const auto w = reinterpret_cast<QWidget *>(sender());
    qDebug() << "onDACCCChange " << w << num;
    const auto dacNumber = dacNumberFromParamName(w);
    if (dacNumber < 4)
    {
        const auto paramName = w->property("objectName").toString();
        auto &dac = Configuration::instance().getDac(dacNumber);
        uint8_t cc_msb = dac.cc_msb;
        uint8_t cc_lsb = dac.cc_lsb;
        if (paramName.contains("MSB"))
        {
            cc_msb = num;
        }
        if (paramName.contains("LSB"))
        {
            cc_lsb = num;
        }
        if (dac.cc_msb != cc_msb)
        {
            dac.setCCMSB(cc_msb);
        }
        if (dac.cc_lsb != cc_lsb)
        {
            dac.setCCLSB(cc_lsb);
        }
    }
}

uint8_t psgNumberFromParamName(const QWidget *w)
{
    const auto paramName = w->property("objectName").toString();
    uint8_t psgNumber = 255;
    if (paramName.contains("PSGA"))
    {
        psgNumber = 0;
    }
    if (paramName.contains("PSGB"))
    {
        psgNumber = 1;
    }
    if (paramName.contains("PSGC"))
    {
        psgNumber = 2;
    }
    if (paramName.contains("PSGN"))
    {
        psgNumber = 3;
    }
    return psgNumber;
}

void MainWindow::onPSGChannelChange(const QString &num)
{
    const auto w = reinterpret_cast<QWidget *>(sender());
    qDebug() << "onPSGChannelChange " << w << num;
    const auto psgNumber = psgNumberFromParamName(w);
    if (psgNumber < 4)
    {
        const auto channel = static_cast<uint8_t>(num.toUInt());
        auto &psg = Configuration::instance().getPsg(psgNumber);
        if (psg.channel != channel)
        {
            psg.setChannel(channel);
        }
    }
}

void MainWindow::onPSGModeChange(const bool &sel)
{
    const auto w = reinterpret_cast<QRadioButton *>(sender());
    qDebug() << "onPSGModeChange " << w << sel;
    const auto psgNumber = psgNumberFromParamName(w);
    if (psgNumber < 4)
    {
        const auto paramName = w->property("objectName").toString();
        auto &psg = Configuration::instance().getPsg(psgNumber);
        auto mode = psg.mode;
        if (paramName.contains("Note") && w->isChecked())
        {
            mode = DataModel::MidiMode::NOTE_VALUE;
        }
        if (paramName.contains("Velocity") && w->isChecked())
        {
            mode = DataModel::MidiMode::NOTE_VELOCITY;
        }
        if (paramName.contains("CC7") && w->isChecked())
        {
            mode = DataModel::MidiMode::CC_7;
        }
        if (paramName.contains("CC14") && w->isChecked())
        {
            mode = DataModel::MidiMode::CC_14;
        }
        if (psg.mode != mode)
        {
            psg.setMode(mode);
        }
    }
}

void MainWindow::onPSGChannelEnableChanged(const bool &state)
{
    const auto w = reinterpret_cast<QWidget *>(sender());
    qDebug() << "onPSGChannelEnableChanged " << w << state;
    const auto psgNumber = psgNumberFromParamName(w);
    if (psgNumber < 4)
    {
        auto &psg = Configuration::instance().getPsg(psgNumber);
        if (psg.enableChannel != state)
        {
            psg.setEnableChannel(state);
        }
    }
}

uint8_t psgMixNumFromParamName(const QWidget *w)
{
    const auto paramName = w->property("objectName").toString();
    if (paramName.contains("PSGNA"))
    {
        return 0;
    }
    if (paramName.contains("PSGNB"))
    {
        return 1;
    }
    if (paramName.contains("PSGNC"))
    {
        return 2;
    }
    return 255;
}

void MainWindow::onPSGNMixEnableChanged(const bool &state)
{
    const auto w = reinterpret_cast<QWidget *>(sender());
    const auto psgNumber = psgNumberFromParamName(w);
    const auto mixNum = psgMixNumFromParamName(w);
    qDebug() << "onPSGNMixEnableChanged " << w << state << psgNumber << mixNum;
    if (psgNumber < 4 && mixNum < 3)
    {
        auto &psg = Configuration::instance().getPsg(psgNumber);
        if (mixNum == 0 && psg.mixtoA != state)
        {
            psg.setMixtoA(state);
            return;
        }
        if (mixNum == 1 && psg.mixtoB != state)
        {
            psg.setMixtoB(state);
            return;
        }
        if (mixNum == 2 && psg.mixtoC != state)
        {
            psg.setMixtoC(state);
            return;
        }
    }
}

void MainWindow::onPSGCCChange(const int &num)
{
    const auto w = reinterpret_cast<QWidget *>(sender());
    qDebug() << "onPSGCCChange " << w << num;
    const auto psgNumber = psgNumberFromParamName(w);
    if (psgNumber < 4)
    {
        auto &psg = Configuration::instance().getPsg(psgNumber);
        if (psg.cc_msb != num)
        {
            psg.setCCMSB(num);
        }
    }
}

void MainWindow::onDacChanged()
{
    const auto dac = reinterpret_cast<DataModel::DAC *>(sender());
    qDebug() << "Received DAC model change" << dac->name;
    QComboBox *uiChannel = nullptr;
    QCheckBox *uiEnabled = nullptr;
    QCheckBox *uiGate = nullptr;
    QCheckBox *uiTrig = nullptr;
    QRadioButton *uiGain1 = nullptr;
    QRadioButton *uiGain2 = nullptr;
    QRadioButton *uiNote = nullptr;
    QRadioButton *uiVelocity = nullptr;
    QRadioButton *uiCC7 = nullptr;
    QRadioButton *uiCC14 = nullptr;
    QSpinBox *uiRangeMin = nullptr;
    QSpinBox *uiRangeMax = nullptr;
    QSpinBox *uiCCMSB = nullptr;
    QSpinBox *uiCCLSB = nullptr;

    int dacNumber = 255;

    if (dac->name == "DACA")
    {
        dacNumber = 0;
        uiChannel = ui->cmbDACAChannel;
        uiEnabled = ui->cbDACAEnableChannel;
        uiGate = ui->cbDACAEnableGate;
        uiTrig = ui->cbDACAEnableTrig;
        uiGain1 = ui->rbDACAGain1;
        uiGain2 = ui->rbDACAGain2;
        uiNote = ui->rbDACANote;
        uiVelocity = ui->rbDACAVelocity;
        uiCC7 = ui->rbDACACC7;
        uiCC14 = ui->rbDACACC14;
        uiRangeMin = ui->sbDACARangeMin;
        uiRangeMax = ui->sbDACARangeMax;
        uiCCMSB = ui->sbDACACCMSB;
        uiCCLSB = ui->sbDACACCLSB;
    }
    if (dac->name == "DACB")
    {
        dacNumber = 1;
        uiChannel = ui->cmbDACBChannel;
        uiEnabled = ui->cbDACBEnableChannel;
        uiGate = ui->cbDACBEnableGate;
        uiTrig = ui->cbDACBEnableTrig;
        uiGain1 = ui->rbDACBGain1;
        uiGain2 = ui->rbDACBGain2;
        uiNote = ui->rbDACBNote;
        uiVelocity = ui->rbDACBVelocity;
        uiCC7 = ui->rbDACBCC7;
        uiCC14 = ui->rbDACBCC14;
        uiRangeMin = ui->sbDACBRangeMin;
        uiRangeMax = ui->sbDACBRangeMax;
        uiCCMSB = ui->sbDACBCCMSB;
        uiCCLSB = ui->sbDACBCCLSB;
    }
    if (dac->name == "DACC")
    {
        dacNumber = 2;
        uiChannel = ui->cmbDACCChannel;
        uiEnabled = ui->cbDACCEnableChannel;
        uiGate = ui->cbDACCEnableGate;
        uiTrig = ui->cbDACCEnableTrig;
        uiGain1 = ui->rbDACCGain1;
        uiGain2 = ui->rbDACCGain2;
        uiNote = ui->rbDACCNote;
        uiVelocity = ui->rbDACCVelocity;
        uiCC7 = ui->rbDACCCC7;
        uiCC14 = ui->rbDACCCC14;
        uiRangeMin = ui->sbDACCRangeMin;
        uiRangeMax = ui->sbDACCRangeMax;
        uiCCMSB = ui->sbDACCCCMSB;
        uiCCLSB = ui->sbDACCCCLSB;
    }
    if (dac->name == "DACD")
    {
        dacNumber = 3;
        uiChannel = ui->cmbDACDChannel;
        uiEnabled = ui->cbDACDEnableChannel;
        uiGate = ui->cbDACDEnableGate;
        uiTrig = ui->cbDACDEnableTrig;
        uiGain1 = ui->rbDACDGain1;
        uiGain2 = ui->rbDACDGain2;
        uiNote = ui->rbDACDNote;
        uiVelocity = ui->rbDACDVelocity;
        uiCC7 = ui->rbDACDCC7;
        uiCC14 = ui->rbDACDCC14;
        uiRangeMin = ui->sbDACDRangeMin;
        uiRangeMax = ui->sbDACDRangeMax;
        uiCCMSB = ui->sbDACDCCMSB;
        uiCCLSB = ui->sbDACDCCLSB;
    }

    if (uiChannel)
    {
        uiChannel->setCurrentIndex(dac->channel - 1);
    }
    if (uiEnabled)
    {
        uiEnabled->setChecked(dac->enableChannel);
    }
    if (uiGate)
    {
        uiGate->setChecked(dac->enableGate);
    }
    if (uiTrig)
    {
        uiTrig->setChecked(dac->enableTrig);
    }
    if (uiGain1)
    {
        uiGain1->setChecked(dac->enableGain2x == false);
    }
    if (uiGain2)
    {
        uiGain2->setChecked(dac->enableGain2x == true);
    }
    if (uiNote)
    {
        uiNote->setChecked(dac->mode == DataModel::MidiMode::NOTE_VALUE);
    }
    if (uiVelocity)
    {
        uiVelocity->setChecked(dac->mode == DataModel::MidiMode::NOTE_VELOCITY);
    }
    if (uiCC7)
    {
        uiCC7->setChecked(dac->mode == DataModel::MidiMode::CC_7);
    }
    if (uiCC14)
    {
        uiCC14->setChecked(dac->mode == DataModel::MidiMode::CC_14);
    }
    if (uiRangeMin)
    {
        uiRangeMin->setValue((int)dac->range_min);
    }
    if (uiRangeMax)
    {
        uiRangeMax->setValue((int)dac->range_max);
    }
    if (uiCCMSB)
    {
        uiCCMSB->setValue((int)dac->cc_msb);
    }
    if (uiCCLSB)
    {
        uiCCLSB->setValue((int)dac->cc_lsb);
    }
}

void MainWindow::onPsgChanged()
{
    const auto psg = reinterpret_cast<DataModel::PSG *>(sender());
    qDebug() << "Received PSG model change" << psg->name;
    QComboBox *uiChannel = nullptr;
    QCheckBox *uiEnabled = nullptr;
    QRadioButton *uiNote = nullptr;
    QRadioButton *uiVelocity = nullptr;
    QRadioButton *uiCC7 = nullptr;
    QSpinBox *uiCCMSB = nullptr;
    QCheckBox *uiMixtoA = nullptr;
    QCheckBox *uiMixtoB = nullptr;
    QCheckBox *uiMixtoC = nullptr;

    if (psg->name == "PSGA")
    {
        uiChannel = ui->cmbPSGAChannel;
        uiEnabled = ui->cbPSGAEnableChannel;
    }
    if (psg->name == "PSGB")
    {
        uiChannel = ui->cmbPSGBChannel;
        uiEnabled = ui->cbPSGBEnableChannel;
    }
    if (psg->name == "PSGC")
    {
        uiChannel = ui->cmbPSGCChannel;
        uiEnabled = ui->cbPSGCEnableChannel;
    }
    if (psg->name == "PSGN")
    {
        uiChannel = ui->cmbPSGNChannel;
        uiEnabled = ui->cbPSGNEnableChannel;

        uiNote = ui->rbPSGNNote;
        uiVelocity = ui->rbPSGNVelocity;
        uiCC7 = ui->rbPSGNCC7;
        uiCCMSB = ui->sbPSGNCC;
        uiMixtoA = ui->cbPSGNA;
        uiMixtoB = ui->cbPSGNB;
        uiMixtoC = ui->cbPSGNC;
    }

    if (uiChannel)
    {
        uiChannel->setCurrentIndex(psg->channel - 1);
    }
    if (uiEnabled)
    {
        uiEnabled->setChecked(psg->enableChannel);
    }
    if (uiNote)
    {
        uiNote->setChecked(psg->mode == DataModel::MidiMode::NOTE_VALUE);
    }
    if (uiVelocity)
    {
        uiVelocity->setChecked(psg->mode == DataModel::MidiMode::NOTE_VELOCITY);
    }
    if (uiCC7)
    {
        uiCC7->setChecked(psg->mode == DataModel::MidiMode::CC_7);
    }
    if (uiCCMSB)
    {
        uiCCMSB->setValue((int)psg->cc_msb);
    }
    if (uiMixtoA)
    {
        uiMixtoA->setChecked(psg->mixtoA);
    }
    if (uiMixtoB)
    {
        uiMixtoB->setChecked(psg->mixtoB);
    }
    if (uiMixtoC)
    {
        uiMixtoC->setChecked(psg->mixtoC);
    }
}

void MainWindow::displaySysEx(const QByteArray &data)
{
    ui->txtMidiSyxEx->setPlainText(data.toHex(' '));
}
