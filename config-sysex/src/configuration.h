#pragma once

#include <QDebug>
#include <QObject>
#include <QSettings>

namespace DataModel {

enum MidiMode
{
    NOTE_VALUE,
    NOTE_VELOCITY,
    CC_7,
    CC_14
};

class PSC : public QObject
{
    Q_OBJECT

public:
    explicit PSC(QObject *parent = nullptr);

};

class DAC : public QObject
{
    Q_OBJECT

public:
    explicit DAC(QObject *parent = nullptr);

    QString serialiseToConfigString(const uint8_t &index) const;
    void deserialiseConfigString(const QString &k, const QString &v);

    void setChannel(const uint8_t &newchannel);
    void setEnableChannel(const bool &enabled);
    void setEnablePitchbend(const bool &enabled);
    void setEnableGate(const bool &enabled);
    void setEnableTrig(const bool &enabled);
    void setEnableGain2x(const bool &enabled);
    void setMode(const MidiMode &newmode);
    void setRangeMin(const uint8_t &newmin);
    void setRangeMax(const uint8_t &newmax);
    void setCCMSB(const uint8_t &newmsb);
    void setCCLSB(const uint8_t &newlsb);

signals:
    void changed();

public:
    // static props
    QString name;

    // persistent props
    uint8_t channel;
    bool enableChannel;
    bool enablePitchbend;
    bool enableGate;
    bool enableTrig;
    bool enableGain2x;
    MidiMode mode;
    uint8_t range_min;
    uint8_t range_max;
    uint8_t cc_msb;
    uint8_t cc_lsb;

    // transient props
    uint8_t msb_value;
};

class PSG : public QObject
{
    Q_OBJECT

public:
    explicit PSG(QObject *parent = nullptr);

    QString serialiseToConfigString(const uint8_t &index) const;
    void deserialiseConfigString(const QString &k, const QString &v);

    void setChannel(const uint8_t &newchannel);
    void setEnableChannel(const bool &enabled);
    void setMode(const MidiMode &newmode);
    void setCCMSB(const uint8_t &newmsb);
    void setMixtoA(const bool &enabled);
    void setMixtoB(const bool &enabled);
    void setMixtoC(const bool &enabled);

signals:
    void changed();

public:
    // static props
    QString name;

    // persistent props
    uint8_t channel;
    bool enableChannel;

    // PSGN only
    MidiMode mode;
    uint8_t cc_msb;
    bool mixtoA;
    bool mixtoB;
    bool mixtoC;
};

} // namespace DataModel

class Configuration : public QObject
{
    Q_OBJECT
public:
    explicit Configuration(QObject *parent = nullptr);

    // Singleton
    // https://codereview.stackexchange.com/questions/197486
    Configuration(const Configuration&) = delete;
    Configuration(Configuration&&) = delete;
    Configuration& operator=(const Configuration&) = delete;
    Configuration& operator=(Configuration&&) = delete;
    static Configuration& instance()
    {
        static Configuration _instance;
        return _instance;
    }

    // Getters

    DataModel::PSC& getPSC() {
        return m_psc;
    }

    DataModel::DAC& getDac(const uint8_t &dac) {
        return m_dacs[dac];
    }

    DataModel::PSG& getPsg(const uint8_t &psg) {
        return m_psgs[psg];
    }

public slots:

    // Save/Load

    void loadFromSettings();
    void serialiseAndSave();
    void serialiseToSysEx();
    void deserialise(const QString &config);

signals:
    void sysExData(const QByteArray &data);

private:
    DataModel::PSC m_psc;
    DataModel::DAC m_dacs[4];
    DataModel::PSG m_psgs[4];
};
