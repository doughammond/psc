#include "pscapp.h"

PSCApp::PSCApp()
    : QObject()
    , m_midi(new MIDIManager(this))
    , m_ui()
{
    const auto &cfg = Configuration::instance();
    connect(&cfg, &Configuration::sysExData, m_midi, &MIDIManager::sendSysEx);
}

PSCApp::~PSCApp()
{
}

void PSCApp::show()
{
    const auto &cfg = Configuration::instance();
    connect(&cfg, &Configuration::sysExData, &m_ui, &MainWindow::displaySysEx);
    connect(&m_ui, &MainWindow::midiDeviceSelectionChanged, m_midi, &MIDIManager::openMidiDevice);
    m_ui.setMidiDeviceNames(m_midi->listMidiDevices());
    m_ui.show();
}

