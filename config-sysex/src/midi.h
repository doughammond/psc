#pragma once

#include <QObject>
#include <QDebug>

#define DONT_SET_USING_JUCE_NAMESPACE 1
#define JUCE_DONT_DECLARE_PROJECTINFO 1
#include <JuceHeader.h>
#include <juce_audio_devices/juce_audio_devices.h>

class MIDIManager : public QObject
{
    Q_OBJECT

public:
    explicit MIDIManager(QObject *parent = nullptr);
    virtual ~MIDIManager();

    QStringList listMidiDevices();

public slots:
    void openMidiDevice(const int index);
    void sendSysEx(const QByteArray &data);

private:
    void closeMidiDevice();

    juce::MidiOutput* m_midiOut = nullptr;
};
