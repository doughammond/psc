QT += core gui widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    ../misc-lib/JuceLibraryCode/AppConfig.h \
    ../misc-lib/JuceLibraryCode/JuceHeader.h \
    \
    src/configuration.h \
    src/midi.h \
    src/pscapp.h \
    src/ui/mainwindow.h

SOURCES += \
    ../misc-lib/JuceLibraryCode/include_juce_audio_basics.cpp \
    ../misc-lib/JuceLibraryCode/include_juce_audio_devices.cpp \
    ../misc-lib/JuceLibraryCode/include_juce_audio_formats.cpp \
    ../misc-lib/JuceLibraryCode/include_juce_core.cpp \
    ../misc-lib/JuceLibraryCode/include_juce_data_structures.cpp \
    ../misc-lib/JuceLibraryCode/include_juce_events.cpp \
    \
    src/configuration.cpp \
    src/midi.cpp \
    src/main.cpp \
    src/pscapp.cpp \
    src/ui/mainwindow.cpp

FORMS += \
    src/ui/mainwindow.ui

#RESOURCES += \
#    res/qml.qrc

win32:DEFINES += -D_WIN32_WINNT=0x0600
win32:LIBS += -lwinmm -lole32

unix:DEFINES += JUCE_LINUX JUCE_ALSA=1 JUCE_ALSA_MIDI_NAME=\\\"PSC\\\"
unix:LIBS += -ldl -lasound

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

INCLUDEPATH += ../misc-lib/JuceLibraryCode
INCLUDEPATH += ../misc-lib/JuceLibraryCode/modules
